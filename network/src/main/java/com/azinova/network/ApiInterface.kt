package com.azinova.network

import com.azinova.model.input.InputCreateCustomer
import com.azinova.model.input.InputLogin
import com.azinova.model.input.order.InputOrderItems
import com.azinova.model.response.create_customer.CreateCustomerResponse
import com.azinova.model.response.customers_list.CustomerListResponse
import com.azinova.model.response.data.ResponseGetData
import com.azinova.model.response.login.LoginResponse
import com.azinova.model.response.order.ResponseOrderItems
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface ApiInterface {

    @POST("/auth")
    suspend fun loginApiCall(@Body inputLogin: InputLogin?): LoginResponse

    @GET("/get_products_categs")
    suspend fun getData():  ResponseGetData

    @POST("/create_customer")
    suspend fun createCustomerApi(@Body inputCreateCustomer: InputCreateCustomer?): CreateCustomerResponse

    @GET("/get_customers")
    suspend fun getCustomerList(): CustomerListResponse

    @POST("/create_order")
    suspend fun CreateOrder(@Body inputOrderItems: InputOrderItems):ResponseOrderItems
}