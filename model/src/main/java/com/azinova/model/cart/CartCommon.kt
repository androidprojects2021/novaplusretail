package com.azinova.model.cart

import com.azinova.model.response.data.ProductsItem
import java.util.*

data class CartCommon(
    var total: String? = "",

    var selected_pos: Int? = -1,

    var selected_cartid: Int? = 0,

    var selected_item_details: ItemDetails? = null


)

data class ItemDetails(
    var cart_id: Int? = null,

    var item_id: Int? = null,

    var item_name: String? = null,

    var price: Double? = null,

    var cost: Double? = null,

    var tax: Int? = null,

    var cartQty: Int? = null,

    var discount: Int? = 0,

    var customer_id: Int? = null,

    var customer_name: String? = null,

    var available_qty:Int? = null,

    var total: Double? = null
)