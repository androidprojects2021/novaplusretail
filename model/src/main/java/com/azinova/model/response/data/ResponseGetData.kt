package com.azinova.model.response.data

import com.google.gson.annotations.SerializedName
import java.util.*

data class ResponseGetData(

	@field:SerializedName("categories")
	val categories: ArrayList<CategoriesItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("products")
	val products: ArrayList<ProductsItem?>? = null
)

data class AttributesItem(

	@field:SerializedName("attribute")
	val attribute: String? = null,

	@field:SerializedName("value")
	val value: String? = null
)

data class CategoriesItem(

	@field:SerializedName("parent_id")
	val parentId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class ProductsItem(

	@field:SerializedName("cost")
	val cost: Double? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("attributes")
	val attributes: List<AttributesItem?>? = null,

	@field:SerializedName("tax")
	val tax: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("category")
	val category: Int? = null,

	@field:SerializedName("barcode")
	val barcode: String? = null,

	@field:SerializedName("qty_available")
	val quantity: Int? = null

)
