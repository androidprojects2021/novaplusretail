package com.azinova.model.response.customers_list

import com.google.gson.annotations.SerializedName
import java.util.*

data class CustomerListResponse(

	@field:SerializedName("customers")
	val customers: ArrayList<CustomersItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class CustomersItem(

	@field:SerializedName("phone")
	val landphone: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
)
