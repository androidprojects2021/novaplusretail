package com.azinova.model.response

import com.azinova.model.response.data.AttributesItem
import com.google.gson.annotations.SerializedName
import java.util.*

data class ProductItemSection(

    @field:SerializedName("cost")
    val cost: Double? = null,

    @field:SerializedName("code")
    val code: String? = null,

    @field:SerializedName("image_url")
    val imageUrl: String? = null,

    @field:SerializedName("price")
    val price: Double? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("category")
    val category: Int? = null
)
