package com.azinova.model.response.login

import com.google.gson.annotations.SerializedName

data class LoginResponse(

    @field:SerializedName("result")
    val result: Result? = null,

    @field:SerializedName("id")
    val id: Any? = null,

    @field:SerializedName("jsonrpc")
    val jsonrpc: String? = null
)

data class Details(


    @field:SerializedName("id")
    val id: Int? = null,


    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("username")
    val username: String? = null
)

data class Result(

    @field:SerializedName("details")
    val details: Details? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
