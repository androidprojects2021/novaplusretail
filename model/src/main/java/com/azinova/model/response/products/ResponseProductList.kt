package com.azinova.model.response.products

import com.google.gson.annotations.SerializedName

data class ResponseProductList(

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("products")
	val products: List<ProductsItem?>? = null
)

data class ProductsItem(

	@field:SerializedName("cost")
	val cost: Double? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("attributes")
	val attributes: List<Any?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("category")
	val category: Int? = null
)

data class AttributesItem(

	@field:SerializedName("Color")
	val color: String? = null,

	@field:SerializedName("Legs")
	val legs: String? = null
)
