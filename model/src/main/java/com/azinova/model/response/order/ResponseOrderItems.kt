package com.azinova.model.response.order

import com.google.gson.annotations.SerializedName

data class ResponseOrderItems(

	@field:SerializedName("result")
	val result: Result? = null,

	@field:SerializedName("id")
	val id: Any? = null,

	@field:SerializedName("jsonrpc")
	val jsonrpc: String? = null
)

data class Result(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("invoice")
	val invoice: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("invoice_name")
	val invoice_id: String? = null,

	@field:SerializedName("order_date")
	val order_date: String? = null


)
