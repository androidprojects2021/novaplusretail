package com.azinova.model.response

import com.google.gson.annotations.SerializedName

data class ResponseCategoryList(

	@field:SerializedName("categories")
	val categories: List<CategoriesItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class CategoriesItem(

	@field:SerializedName("parent_id")
	val parentId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
