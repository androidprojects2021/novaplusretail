package com.azinova.model.input.order

import com.google.gson.annotations.SerializedName

data class InputOrderItems(

	@field:SerializedName("amount_total_bank")
	var amountTotalBank: Double? = null,

	@field:SerializedName("user_id")
	var userId: Int? = null,

	@field:SerializedName("amount_total_cash")
	var amountTotalCash: Double? = null,

	@field:SerializedName("customer_id")
	var customerId: Int? = null,

	@field:SerializedName("products")
	var products: ArrayList<ProductsItem?>? = null
)

data class ProductsItem(

	@field:SerializedName("price_unit")
	val priceUnit: Double? = null,

	@field:SerializedName("product_id")
	val productId: Int? = null,

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("discount")
	val discount: Int? = null
)
