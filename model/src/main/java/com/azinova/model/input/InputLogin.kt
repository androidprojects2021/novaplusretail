package com.azinova.model.input

import com.google.gson.annotations.SerializedName

class InputLogin(
	@field:SerializedName("username")
	var username: String? = null,
	@field:SerializedName("password")
	var password: String? = null
)
