package com.azinova.model.input

import com.google.gson.annotations.SerializedName

class InputCreateCustomer(
    @field:SerializedName("name")
    var name: String? = null,
    @field:SerializedName("mobile")
    var mobile: String? = null,
    @field:SerializedName("phone")
    var phone: String? = null,
    @field:SerializedName("email")
    var email: String? = null,
    @field:SerializedName("user_id")
    var user_id: Int? = null

)

