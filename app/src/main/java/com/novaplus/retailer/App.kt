package com.novaplus.retailer

import android.app.Application
import com.azinova.common.network.NetworkConstants
import com.azinova.network.ApiClient
import com.azinova.preference.SharedPrefs

class App:Application() {

    val sharedPrefs = SharedPrefs
    val retrofit = ApiClient.getRetrofit(NetworkConstants.BaseUrl)

    private var mInstance: App? = null
    private var bus: RxBus? = null

    override fun onCreate() {
        super.onCreate()
        SharedPrefs.getInstance(applicationContext)

        bus = RxBus()
        mInstance = this
    }

    @Synchronized
    fun getInstance(): App? {
        return mInstance
    }

    fun bus(): RxBus? {
        return bus
    }

}