package com.novaplus.retailer.view.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.CategoryTable

class CategoryAdapter(val requireContext: Context,val categorylist: List<CategoryTable>,
                      val itemSelected: (CategoryTable?) -> Unit) :RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private var selectedPosition = 0

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_categorys, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryAdapter.ViewHolder, position: Int) {
        holder.textView_category.text = categorylist[position].category_name

        if (position == selectedPosition) {
            itemSelected.invoke(categorylist.get(position))
            holder.textView_category!!.setTextColor(ContextCompat.getColor(requireContext, R.color.white))
            holder.cardview.setBackground(requireContext.getResources().getDrawable( R.drawable.bg_selected))


        } else {
            holder.textView_category!!.setTextColor(ContextCompat.getColor(requireContext, R.color.colorBlack))
            holder.cardview.setBackground(requireContext.getResources().getDrawable(R.drawable.rectangle_shapeborder))


        }



        holder.itemView.setOnClickListener { v: View? ->
            onStateChangedListener(position, holder)
        }
    }

    private fun onStateChangedListener(position: Int, holder: CategoryAdapter.ViewHolder) {
        selectedPosition = position
        itemSelected.invoke(categorylist.get(position))
        holder.textView_category!!.setTextColor(ContextCompat.getColor(requireContext, R.color.white))
        holder.cardview.setBackground(requireContext.getResources().getDrawable( R.drawable.bg_selected))
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int {
        return categorylist.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val   textView_category = itemView.findViewById<TextView?>(R.id.textView_category)
        val  cardview = itemView.findViewById<CardView>(R.id.cardview)
    }
}