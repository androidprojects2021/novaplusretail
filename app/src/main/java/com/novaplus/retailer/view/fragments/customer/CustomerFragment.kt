package com.novaplus.retailer.view.fragments.customer

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.commons.Loader
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.R
import com.novaplus.retailer.utils.ActivityUtils
import com.novaplus.retailer.view.adapter.CustomerAdapter
import com.novaplus.retailer.view.fragments.payment.PaymentFragment
import kotlinx.android.synthetic.main.activity_bottom_host.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.create_customer.*
import kotlinx.android.synthetic.main.fragment_customer.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class CustomerFragment : Fragment() {
    private lateinit var customerViewModel: CustomerViewModel
    lateinit var customerAdapter: CustomerAdapter

    var getallcustomers = ArrayList<CustomersItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_customer, container, false)
    }

    override fun onResume() {
        super.onResume()
        activity?.ll_menu?.visibility = View.VISIBLE
        activity?.ll_search?.visibility = View.VISIBLE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        customerViewModel = ViewModelProvider(this).get(CustomerViewModel::class.java)

        init()
        onclick()
        onObserve()
    }

    private fun onclick() {
        img_add.setOnClickListener {

            val createcustomerFragment: Fragment = CreateCustomerFragment()
            ActivityUtils.replaceFragment(
                parentFragmentManager,
                createcustomerFragment,
                R.id.main_content
            )
        }
    }

    private fun init() {

        rv_customerlist.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        )
        GlobalScope.launch(Dispatchers.Main) {
            if (requireContext().isConnectedToNetwork()) {
                Loader.showLoader(requireContext())
                customerViewModel.getAllCustomers()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Please connect to internet",
                    Toast.LENGTH_LONG
                ).show()
            }

        }
    }


    private fun onObserve() {
        customerViewModel.getAllCustomers.observe(viewLifecycleOwner, Observer { CustomerList ->
            Loader.hideLoader()
            if (CustomerList.status.equals("success")) {
                Log.d("Customers ", "GetCustomers: " + CustomerList.customers)
                getallcustomers.clear()
                getallcustomers = CustomerList.customers!!
                customerAdapter =
                    CustomerAdapter(requireContext(), CustomerList.customers)
                rv_customerlist.adapter = customerAdapter
            } else {
                Toast.makeText(requireContext(), CustomerList.message, Toast.LENGTH_LONG).show()

            }
        })


    }

    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}