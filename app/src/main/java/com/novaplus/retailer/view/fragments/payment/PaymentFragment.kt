package com.novaplus.retailer.view.fragments.payment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.RemoteException
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.commons.Loader
import com.azinova.model.input.order.InputOrderItems
import com.azinova.model.input.order.ProductsItem
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.utils.ActivityUtils
import com.novaplus.retailer.view.adapter.ConfirmCartAdapter
import com.novaplus.retailer.view.adapter.SelectCustomerAdapter
import com.novaplus.retailer.view.fragments.dashboard.DashboardFragment
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_payment.*
import kotlinx.android.synthetic.main.fragment_print_recipt.*
import kotlinx.android.synthetic.main.fragment_print_recipt.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import woyou.aidlservice.jiuiv5.ICallback
import woyou.aidlservice.jiuiv5.ITax
import woyou.aidlservice.jiuiv5.IWoyouService
import java.lang.Exception

@InternalCoroutinesApi
class PaymentFragment() : Fragment() {
    lateinit var paymentViewmodel: PaymentViewmodel

    var param1: String? = ""
    var Totalamount: String? = ""
    var Subtotal: String? = ""
    var Tax: String? = ""
    var Discount: String? = ""
    var payment_type: Int = 1

    private val woyouService: IWoyouService? = null
    private val callback2: ICallback? = null

    lateinit var selectcustomeradapter: SelectCustomerAdapter
    private lateinit var rv_customerlist: RecyclerView

    lateinit var confirmCartAdapter: ConfirmCartAdapter

    var seletedcustomer_details: ArrayList<CustomersItem>? = null

    var cartlist: ArrayList<CartItemTable>? = ArrayList()
    var productlist: ArrayList<ProductsItem?>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        paymentViewmodel = ViewModelProvider(this).get(PaymentViewmodel::class.java)

        arguments?.let {
            param1 = arguments!!.getString("from")
            Log.d("Test", "init: " + param1)

            Totalamount = arguments!!.getString("Total Amount")
            Subtotal = arguments!!.getString("Subtotal")
            Tax = arguments!!.getString("Tax")
            Discount = arguments!!.getString("Discount")

        }

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Log.d("TAG", "Fragment back pressed invoked")


                    val dashboardFragment: Fragment = DashboardFragment()
                    ActivityUtils.replaceFragmentAddtoBackStack(
                        parentFragmentManager,
                        dashboardFragment,
                        R.id.main_content
                    )
                }
            }
            )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().ll_menu.visibility = View.GONE
        payableamount.text = "AED " + Totalamount
        initview()
        onclick()
        onObserve()
    }

    private fun onObserve() {
        paymentViewmodel.createorderStatus.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if (it.result?.status.equals("success")) {
                Log.d("Order ", "Order Response: " + it.result)
                Toast.makeText(requireContext(), it.result?.message, Toast.LENGTH_LONG).show()

                ReceiptDialog()
            } else {
                Toast.makeText(requireContext(), it.result?.message, Toast.LENGTH_LONG).show()

            }
        })

        paymentViewmodel.getAllCustomers.observe(viewLifecycleOwner, Observer { CustomerList ->
            Loader.hideLoader()
            if (CustomerList.status.equals("success")) {
                Log.d("Customers ", "GetCustomers: " + CustomerList.customers)

                if (CustomerList.customers?.size != 0) {
                    val customerbuilder = AlertDialog.Builder(requireContext())
                    val customerview: View =
                        LayoutInflater.from(requireContext())
                            .inflate(R.layout.dialog_customerlist, null)
                    customerbuilder.setView(customerview)
                    val customeralertDialog: AlertDialog = customerbuilder.create()

                    rv_customerlist = customerview.findViewById(R.id.rv_customerlist)

                    rv_customerlist.setLayoutManager(
                        LinearLayoutManager(
                            context,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                    )
                    selectcustomeradapter =
                        SelectCustomerAdapter(requireContext(), CustomerList.customers) {
                            seletedcustomer_details?.clear()
                            if (it != null) {
                                seletedcustomer_details?.addAll(listOf(it))
                            }
                            /* selected_customer = it?.name.toString()
                             selected_customerId = it?.id!!*/
                            /*Thread {
                                paymentViewmodel.updateCustomer(
                                    requireContext(),
                                    it?.id!!, it.name.toString()
                                )
                            }.start()*/


                            Toasty.success(
                                requireContext(),
                                it?.name + " Selected",
                                Toast.LENGTH_SHORT
                            ).show()

                            customeralertDialog.dismiss()
                        }
                    rv_customerlist.adapter = selectcustomeradapter

                    customeralertDialog.show()
                } else {

                    Toast.makeText(requireContext(), "Customer List is empty", Toast.LENGTH_LONG)
                        .show()
                }


            } else {
                Toast.makeText(requireContext(), CustomerList.message, Toast.LENGTH_LONG).show()

            }
        })

    }

    private fun ReceiptDialog() {

        val builder =
            androidx.appcompat.app.AlertDialog.Builder(requireContext())
        val view =
            LayoutInflater.from(requireContext())
                .inflate(R.layout.fragment_print_recipt, null)
        builder.setView(view)
        val alertDialog = builder.create()
        val btn_close = view.findViewById<RelativeLayout>(R.id.close_checkout)
        val btn_print = view.findViewById<RelativeLayout>(R.id.bill_checkout)
        val btn_invoice = view.findViewById<RelativeLayout>(R.id.btn_invoice)
        val subtotaltext = view.findViewById<TextView>(R.id.bill_subtotaltext)
        val discounttext = view.findViewById<TextView>(R.id.bill_discounttext)
        val totaltext = view.findViewById<TextView>(R.id.bill_totaltext)

        subtotaltext.text = Subtotal
        discounttext.text = Discount + " %"
        totaltext.text = "AED " + Totalamount

        view.rv_orderitems.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        )

        confirmCartAdapter =
            ConfirmCartAdapter(
                requireContext(),
                cartlist

            )
        view.rv_orderitems.adapter = confirmCartAdapter

        btn_close.setOnClickListener {
            alertDialog.dismiss()
            val dashboardFragment: Fragment = DashboardFragment()
            ActivityUtils.replaceFragmentAddtoBackStack(
                parentFragmentManager,
                dashboardFragment,
                R.id.main_content
            )
        }
        btn_print.setOnClickListener {
            PrintReceipt(alertDialog)
            alertDialog.dismiss()
            alertDialog.dismiss()
            val dashboardFragment: Fragment = DashboardFragment()
            ActivityUtils.replaceFragmentAddtoBackStack(
                parentFragmentManager,
                dashboardFragment,
                R.id.main_content
            )
        }

        btn_invoice.setOnClickListener {
            Toasty.warning(requireContext(), "In Progress", Toast.LENGTH_SHORT).show()
        }
        alertDialog.show()


    }


    private fun initview() {
        onpayment_type(payment_type)


        GlobalScope.launch(Dispatchers.Main) {

            paymentViewmodel.getAllCartitems(requireContext())?.observe(viewLifecycleOwner,
                Observer { newcartlist ->
                    try {
                        if (!newcartlist.isNullOrEmpty()) {
                            cartlist?.clear()
                            cartlist?.addAll(newcartlist)
                        }


                    } catch (e: Exception) {
                    }


                })

        }
    }

    private fun onpayment_type(type: Int) {
        when (type) {
            1 -> {
                cashPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_select)
                )
                txt_cash.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                img_cash.setImageResource(R.drawable.ic_cash_white)

                bankPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_nonselect)
                )
                txt_bank.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlack))
                img_bank.setImageResource(R.drawable.ic_debitcard_blue)


            }
            2 -> {
                bankPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_select)
                )
                txt_bank.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                img_bank.setImageResource(R.drawable.ic_debitcard_white)

                cashPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_nonselect)
                )
                txt_cash.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlack))
                img_cash.setImageResource(R.drawable.ic_cash_blue)


            }
            3 -> {

                cashPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_nonselect)
                )
                txt_cash.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlack))

                bankPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_nonselect)
                )
                txt_bank.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlack))



            }
            4 -> {

                cashPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_nonselect)
                )
                txt_cash.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlack))
                bankPay.setBackground(
                    requireContext().getResources().getDrawable(R.drawable.bg_rect_nonselect)
                )
                txt_bank.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorBlack))


            }
            else -> {}
        }
    }


    private fun onclick() {
        ll_back.setOnClickListener {
            val dashboardFragment: Fragment = DashboardFragment()
            ActivityUtils.replaceFragmentAddtoBackStack(
                parentFragmentManager,
                dashboardFragment,
                R.id.main_content
            )
        }

        cashPay.setOnClickListener {
            payment_type = 1
            onpayment_type(payment_type)
        }

        bankPay.setOnClickListener {
            payment_type = 2
            onpayment_type(payment_type)
        }

        debitcardPay.setOnClickListener {
            payment_type = 3
            onpayment_type(payment_type)
        }

        creditcardPay.setOnClickListener {
            payment_type = 4
            onpayment_type(payment_type)
        }

        ll_payment_validate.setOnClickListener {

            GlobalScope.launch(Dispatchers.Main) {

                val inputorder = InputOrderItems()
                when (payment_type) {
                    1 -> {
                        inputorder.amountTotalCash = Totalamount?.toDouble()
                        inputorder.amountTotalBank = 0.00
                    }
                    2 -> {
                        inputorder.amountTotalCash = 0.00
                        inputorder.amountTotalBank = Totalamount?.toDouble()
                    }

                }
                inputorder.userId = App().sharedPrefs.user_id?.toInt()
                if (!cartlist.isNullOrEmpty()) {
                    productlist?.clear()

                    inputorder.customerId = cartlist!![0].customer_id

                    for (item in cartlist!!) {

                        productlist?.add(
                            ProductsItem(
                                priceUnit = item.price,
                                productId = item.item_id,
                                qty = item.cartQty,
                                discount = item.discount
                            )
                        )

                    }
                    inputorder.products = productlist
                }
                Log.d("productlist", "onclick: " + productlist)
                Log.d("All input", "onclick: " + inputorder)

                if (requireContext().isConnectedToNetwork()) {
                    Loader.showLoader(requireContext())
                    paymentViewmodel.paymentValidate(requireContext(), inputorder, cartlist)
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        ll_customerlist.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                if (requireContext().isConnectedToNetwork()) {
                    Loader.showLoader(requireContext())
                    paymentViewmodel.getAllCustomers()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        ll_splitbill.setOnClickListener {
            Toasty.warning(requireContext(), "In Progress", Toast.LENGTH_SHORT).show()
        }
    }




    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

    private fun PrintReceipt(alertDialog: androidx.appcompat.app.AlertDialog) {


        try {


            if (woyouService == null) {
                alertDialog.dismiss()

            }
            try {
                val l = arrayOf("Item", "Qty", "Rate", "Total")
                val width = intArrayOf(25, 5, 8, 8)
                val align = intArrayOf(0, 1, 1, 1)
                val l2 =
                    arrayOf("Invoice : ", "")
                val l3 = arrayOf("Date    : ", "")
                val l4 = arrayOf("Cashier : ", "        ")
                val l5 = arrayOf(
                    "Table   : ", ""
                    // if (selectedName.isEmpty()) selectedTable else selectedName.toString() + ""
                )
                val width2 = intArrayOf(10, 30)
                val align2 = intArrayOf(0, 0)
                woyouService?.setAlignment(1, callback2)
                woyouService?.lineWrap(1, callback2)
                woyouService?.printBitmap(
                    BitmapFactory.decodeResource(
                        resources,
                        R.drawable.pos_waiter_svg_logo
                    ), callback2
                )
                woyouService?.printTextWithFont(" \n", "", 26f, callback2)
                woyouService?.printText("                JVT,Dubai              \n", callback2)
                woyouService?.printText("         04 240 8060, 058 832 6713     \n", callback2)
                woyouService?.printColumnsText(l2, width2, align2, callback2)
                woyouService?.printColumnsText(l3, width2, align2, callback2)
                woyouService?.printColumnsText(l4, width2, align2, callback2)
                woyouService?.printColumnsText(l5, width2, align2, callback2)
                woyouService?.printTextWithFont(
                    "---------------------------------------\n",
                    "",
                    26f,
                    callback2
                )
                woyouService?.printColumnsText(l, width, align, callback2)
                woyouService?.printTextWithFont(
                    "---------------------------------------\n",
                    "",
                    26f,
                    callback2
                )
                /* for (i in cartitems.indices) {
                     var s: String = cartitems.get(i).getCart_name()
                     val qty_: String = cartitems.get(i).getCart_quantity().toString() + ""
                     val price_: String = cartitems.get(i).getCart_price().toString() + ""
                     val total_: Double =
                         cartitems.get(i).getCart_quantity() as Double * cartitems.get(i)
                             .getCart_price()
                     if (cartitems.get(i).getCart_name().length() > 25) {
                         s = cartitems.get(i).getCart_name().substring(0, 25)
                     }
                     val newString =
                         arrayOf(s, qty_, price_, total_.toString())
                     woyouService.printColumnsText(newString, width, align, callback2)
                 }*/

                woyouService?.printTextWithFont(
                    "---------------------------------------\n",
                    "",
                    26f,
                    callback2
                )
                woyouService?.printTextWithFont(
                    "                                     AED \n",
                    "",
                    26f,
                    callback2
                )
                woyouService?.printTextWithFont(
                    "                          Discount : \n",
                    "",
                    26f,
                    callback2
                )
                woyouService?.printTextWithFont(
                    "                             Total : AED \n",
                    "",
                    26f,
                    callback2
                )
                woyouService?.printTextWithFont(" \n", "", 26f, callback2)
                //                    woyouService.setAlignment(0, callback2);
//                    woyouService.printTextWithFont(getBill(), "", 25, callback2);
                woyouService?.printTextWithFont(
                    "             Thank you, Visit again. \n",
                    "",
                    26f,
                    callback2
                )
                // woyouService.cutPaper(callback2)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        } catch (e: Exception) {
        }
    }

    companion object {

        fun newInstance(s: String) =
            PaymentFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

}