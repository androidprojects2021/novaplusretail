package com.novaplus.retailer.view.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.R
import java.util.ArrayList

class SelectCustomerAdapter(val context: Context, val customers: ArrayList<CustomersItem>?, val itemSelected: (CustomersItem?) -> Unit) :
    RecyclerView.Adapter<SelectCustomerAdapter.ViewHolder>() {

    private var selectedPosition = -1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectCustomerAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.card_customers, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectCustomerAdapter.ViewHolder, position: Int) {
        holder.customer.text = customers?.get(position)?.name

        holder.ll_customer.setOnClickListener {
            itemSelected.invoke(customers?.get(position))
        }
    }

    override fun getItemCount(): Int {
        return customers!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val customer = itemView.findViewById<TextView?>(R.id.customer)
        val ll_customer = itemView.findViewById<ConstraintLayout?>(R.id.ll_customer)

    }
}