package com.novaplus.retailer.view.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.ItemTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File

class ProductListAdapter(
    val requireContext: Context, var Productlist: List<ItemTable>,
    val productsListInterface: ProductListInterface
) :RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductListAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.card_productlist,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductListAdapter.ViewHolder, position: Int) {


            if (Productlist.get(position).imageUrl?.isNotEmpty()!!) {


                val savedUri: Uri? = Productlist.get(position).imageUrl?.toUri()

                Glide.with(requireContext)
                    .load(File(savedUri?.getPath()))
                    .placeholder(R.drawable.noproduct_image)
                    .error(R.drawable.noproduct_image)
                    .into(holder.product_img)

            } else {
                Glide.with(requireContext)
                    .load(R.drawable.noproduct_image)
                    .placeholder(R.drawable.noproduct_image)
                    .error(R.drawable.noproduct_image)
                    .into(holder.product_img)
            }
            if (Productlist.get(position).item_name != null) {
                holder.txt_productname.text = Productlist.get(position).item_name

            }
            holder.txt_producttype.text = Productlist.get(position).type
            holder.txt_price.text = "AED " + Productlist.get(position).cost.toString()

            holder.itemView.setOnClickListener {
//                var cartQty = 0
//
//                cartQty = Productlist.get(position).cartQty!! + 1
//            Productlist.get(position).cartQty = cartQty


                productsListInterface.showNotification("Item has been added to cart")

                GlobalScope.launch(Dispatchers.Main) {

                productsListInterface.clickProducts(Productlist[position])

            }



            }




    }

    override fun getItemCount(): Int {
        return Productlist.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val   txt_productname = itemView.findViewById<TextView?>(R.id.txt_productname)
        val  txt_producttype = itemView.findViewById<TextView>(R.id.txt_producttype)
        val  txt_price = itemView.findViewById<TextView>(R.id.txt_price)
        val  product_img = itemView.findViewById<ImageView>(R.id.product_img)
    }

    fun refreshData(items: List<ItemTable>) {
        this.Productlist = items
        notifyDataSetChanged()
    }


    interface ProductListInterface{
        suspend fun clickProducts(get: ItemTable)

        fun showNotification(message: String?)

    }

}