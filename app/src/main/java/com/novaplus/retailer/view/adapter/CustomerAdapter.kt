package com.novaplus.retailer.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.ItemTable
import java.util.ArrayList

class CustomerAdapter(val context: Context, var customers: ArrayList<CustomersItem>?) :
    RecyclerView.Adapter<CustomerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.card_customers, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomerAdapter.ViewHolder, position: Int) {
        holder.customer.text = customers?.get(position)?.name
        holder.email.text = customers?.get(position)?.email
        holder.phone.text = customers?.get(position)?.mobile

    }

    override fun getItemCount(): Int {
        return customers!!.size
    }

    fun refreshData(items: ArrayList<CustomersItem>) {
        this.customers = items
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val customer = itemView.findViewById<TextView?>(R.id.customer_name)
        val email = itemView.findViewById<TextView?>(R.id.customer_email)
        val phone = itemView.findViewById<TextView?>(R.id.customer_phone)

    }
}