package com.novaplus.retailer.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SunmiScanner {
    private static final String TAG = SunmiScanner.class.getSimpleName();
    private Context context;

    public SunmiScanner(Context context) {
        this.context = context;
    }


    public void analysisBroadcast() {
        Log.i(TAG, "analysisBroadcast");
        if (this.isBroadcastOpened) {
            Log.e(TAG, "analysisBroadcast:Broadcast is already opened");
            return;
        }
        if (this.context == null) {
            Log.e(TAG, "analysisBroadcast:context is null");
            return;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.sunmi.scanner.ACTION_DATA_CODE_RECEIVED");
        this.context.registerReceiver(broadcastReceiver, filter);
        isBroadcastOpened = true;
    }

    private boolean isBroadcastOpened = false;
    private static boolean isWaittingResponse = false;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String data = intent.getStringExtra("data");
            if (null != data && data.length() > 0) {
                if (isResponseData(data, DATA_DISCRIBUTE_TYPE.TYPE_BROADCAST)) {
                    Log.i(TAG, "BroadcastReceiver onResponseData:" + data);
                    isWaittingResponse = false;
                    for (Map.Entry<String, OnScannerListener> entry : scannerListenerHashMap.entrySet())
                        entry.getValue().onResponseData(data, DATA_DISCRIBUTE_TYPE.TYPE_BROADCAST);
                } else {
                    Log.i(TAG, "BroadcastReceiver onScanData:" + data);
                    for (Map.Entry<String, OnScannerListener> entry : scannerListenerHashMap.entrySet())
                        entry.getValue().onScanData(data, DATA_DISCRIBUTE_TYPE.TYPE_BROADCAST);
                }
            }
        }
    };

    private boolean isResponseData(String data, DATA_DISCRIBUTE_TYPE type) {
        Log.i(TAG, "isResponseData[hex]:" + data + "[" +ByteUtils.str2HexString(data) + "]");
        switch (type) {
            case TYPE_KEYBOARD:
                data = ByteUtils.hexStr2Str(SerialCmd.RES_PREFIX_HEX).substring(0, 2) + data;
                break;
            case TYPE_BROADCAST:
                break;
            default:
                break;
        }
        return (data.length() > 6 && data.substring(0, 6).equals(ByteUtils.hexStr2Str(SerialCmd.RES_PREFIX_HEX)));
    }


    public void analysisKeyEvent(KeyEvent keyEvent) {
        Log.i(TAG, "analysisKeyEvent:" + keyEvent.toString());
        if (keyEvent == null) {
            Log.e(TAG, "analysisKeyEvent:keyEvent is null.");
            return;
        }
        int unicodeChar = keyEvent.getUnicodeChar();
        sb.append((char) unicodeChar);
        curLen++;
        if (!isKeyEventScanning) {
            isKeyEventScanning = true;
            timerScanCal();
        }
    }

    private static final int SAME_CODE_DELAY_TIME = 200;
    private StringBuilder sb = new StringBuilder();
    private Handler handler = new Handler();
    private int oldLen = 0;
    private int curLen = 0;
    private boolean isKeyEventScanning = false;

    private void timerScanCal() {
        oldLen = curLen;
        handler.postDelayed(scan, SAME_CODE_DELAY_TIME);
    }

    private Runnable scan = new Runnable() {
        @Override
        public void run() {
            if (oldLen != curLen) {
                timerScanCal();
                return;
            }
            oldLen = curLen = 0;
//            String data = sb.toString().replaceAll("\\p{C}", "");
//            String data = sb.toString().replaceAll("[\u0000-\u0009\u000B\u000C\u000E-\u001F\u007F]", "");
            String data = sb.toString();
            if (data.length() > 0) {
                isKeyEventScanning = false;
                if (isResponseData(data, DATA_DISCRIBUTE_TYPE.TYPE_KEYBOARD)) {
                    Log.i(TAG, "KeyEvent onResponseData:" + data);
                    for (Map.Entry<String, OnScannerListener> entry : scannerListenerHashMap.entrySet())
                        entry.getValue().onResponseData(data, DATA_DISCRIBUTE_TYPE.TYPE_KEYBOARD);
                } else {
                    Log.i(TAG, "KeyEvent onScanData:" + data);
                    for (Map.Entry<String, OnScannerListener> entry : scannerListenerHashMap.entrySet())
                        entry.getValue().onScanData(data, DATA_DISCRIBUTE_TYPE.TYPE_KEYBOARD);
                }
                sb.setLength(0);
            }
        }
    };


    public void destory() {
        Log.i(TAG, "destory");
        if (this.context == null || !isBroadcastOpened)
            return;
        try {
            this.context.unregisterReceiver(broadcastReceiver);
            isBroadcastOpened = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setScannerListener(OnScannerListener listener) {
        Log.i(TAG, "setScannerListener:" + listener);
        if (listener == null) {
            Log.e(TAG, "setScannerListener:listener is null.");
            return;
        }
        scannerListenerHashMap.put(listener.toString(), listener);
    }


    public void closeScannerListener(OnScannerListener listener) {
        String key = listener.toString();
        if (scannerListenerHashMap.containsKey(key))
            scannerListenerHashMap.remove(key);
    }


    public void closeScannerListener() {
        scannerListenerHashMap = new ConcurrentHashMap<>();
    }

    public interface OnScannerListener {

        void onScanData(String data, DATA_DISCRIBUTE_TYPE type);

        void onResponseData(String data, DATA_DISCRIBUTE_TYPE type);

        void onResponseTimeout();
    }

    private ConcurrentHashMap<String, OnScannerListener> scannerListenerHashMap = new ConcurrentHashMap<>();


    public void setDataDiscributeType(DATA_DISCRIBUTE_TYPE type) {
        Log.i(TAG, "setDataDiscributeType:" + type.toString());
        if (this.context == null) {
            Log.e(TAG, "setDataDiscributeType:context is null");
            return;
        }
        int usbType = 0;
        boolean bSerialKeyboard = true;
        boolean bSerialBroadcast = true;
        switch (type) {
            case TYPE_KEYBOARD:
                usbType = 0;
                bSerialKeyboard = true;
                bSerialBroadcast = false;
                break;
            case TYPE_BROADCAST:
                usbType = 2;
                bSerialKeyboard = false;
                bSerialBroadcast = true;
                break;
            case TYPE_KEYBOARD_AND_BROADCAST:
                usbType = 0;
                bSerialKeyboard = true;
                bSerialBroadcast = true;
                break;
            default:
                break;
        }

        int pid, vid;
        HashMap<String, UsbDevice> deviceHashMap = ((UsbManager) context.getSystemService(Activity.USB_SERVICE)).getDeviceList();
        for (Map.Entry<String, UsbDevice> entry : deviceHashMap.entrySet()) {
            pid = entry.getValue().getProductId();
            vid = entry.getValue().getVendorId();
            if (pidVidList.contains(pid + "," + vid)) {
                Intent intent = new Intent();
                intent.setAction("com.sunmi.scanner.ACTION_BAR_DEVICES_SETTING");
                intent.putExtra("name", entry.getValue().getDeviceName());
                intent.putExtra("pid", pid);
                intent.putExtra("vid", vid);
                intent.putExtra("type", usbType);
                intent.putExtra("toast", false);
                this.context.sendBroadcast(intent);
            }
        }
        Intent intent = new Intent();
        intent.setAction("com.sunmi.scanner.ACTION_SCANNER_SERIAL_SETTING");
        intent.putExtra("analog_key", bSerialKeyboard);
        intent.putExtra("broadcast", bSerialBroadcast);
        intent.putExtra("toast", false);
        this.context.sendBroadcast(intent);
    }

    public enum DATA_DISCRIBUTE_TYPE {TYPE_KEYBOARD, TYPE_BROADCAST, TYPE_KEYBOARD_AND_BROADCAST}


    private static final List<String> pidVidList = new ArrayList<>(Arrays.asList(
            "4608,1504",
            "9492,1529",
            "34,12879",
            "193,12879"
    ));



    public void sendCommand(String command) {
        Log.i(TAG, "sendCmdByBroadcast[hex]:" + command + "[" + ByteUtils.str2HexString(command) + "]");
        if (this.context == null || command == null || command.isEmpty()) {
            Log.e(TAG, "sendCmdByBroadcast:context or cmd is null!");
            return;
        }
        if (singleThreadExecutor == null)
            singleThreadExecutor = Executors.newSingleThreadExecutor();

        SendThread sendThread = new SendThread(command);
        singleThreadExecutor.execute(sendThread);
    }


    public void sendSimpleCmd(String simpleCmd) {
        if (this.context == null || simpleCmd == null || simpleCmd.isEmpty()) {
            Log.e(TAG, "sendSimpleCmd:context or cmd is null!");
            return;
        }
        String cmd = ByteUtils.hexStr2Str(SerialCmd.PREFIX_HEX + ByteUtils.str2HexString(simpleCmd) + SerialCmd.SUFFIX_HEX);
        sendCommand(cmd);
    }

    private ExecutorService singleThreadExecutor = null;
    private boolean isHaveResponse = false;

    private class SendThread implements Runnable {
        private String strCmd;

        private SendThread(String cmd) {
            strCmd = cmd;
        }

        @Override
        public void run() {
            if (strCmd.length() > 6)
                isHaveResponse = strCmd.substring(0, 6).equals(ByteUtils.hexStr2Str(SerialCmd.PREFIX_HEX));
            else
                isHaveResponse = false;
            isWaittingResponse = true;

            byte[] bytes = strCmd.getBytes();
            byte[] cmd = new byte[bytes.length + 2];
            System.arraycopy(bytes, 0, cmd, 0, bytes.length);
            lrcCheckSum(cmd);
            Intent intent = new Intent("com.sunmi.scanner.Setting_cmd");
            intent.putExtra("cmd_data", cmd);
            if (SunmiScanner.this.context == null)
                return;
            SunmiScanner.this.context.sendBroadcast(intent);

            long curTime = System.currentTimeMillis();
            while (isWaittingResponse) {
                if (!isHaveResponse) {
                    if (System.currentTimeMillis() - curTime > SerialCmd.MIN_SEND_TIME) {
                        Log.i(TAG, "SendThread:NLS cmd has no response");
                        return;
                    }
                } else if (System.currentTimeMillis() - curTime > SerialCmd.MAX_RESPONSE_TIME) {
                    Log.i(TAG, "SendThread:response timeout");
                    for (Map.Entry<String, OnScannerListener> entry : scannerListenerHashMap.entrySet())
                        entry.getValue().onResponseTimeout();
                    return;
                }
            }
            Log.i(TAG, "SendThread:response suc");
        }
    }

    private void lrcCheckSum(byte[] content) {
        int len = content.length;
        int crc = 0;
        for (int l = 0; l < len - 2; l++) {
            crc += content[l] & 0xFF;
        }
        crc = ~crc + 1;
        content[len - 2] = (byte) ((crc >> 8) & 0xFF);
        content[len - 1] = (byte) (crc & 0xFF);
    }
}