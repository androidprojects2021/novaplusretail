package com.novaplus.retailer.view.fragments.carts

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.novaplus.retailer.database.repository.CartRespository
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.database.tables.MulticartTable
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class CartViewModel : ViewModel() {
    var cartItemListFromDb: List<CartItemTable>? = null

    var multicartdetailsByIdFromDb: LiveData<List<MulticartTable>?>? = null
    //var multicartdetailsByIdFromDb: List<MulticartTable>? = null

    var multicartListFromDb: LiveData<List<Int>?>? = null
    var multicartListByIdFromDb: LiveData<List<Int>?>? = null

    fun getAllCartitems(requireContext: Context): List<CartItemTable>? {
        cartItemListFromDb = CartRespository.getCartItems(requireContext)
        return cartItemListFromDb
    }
    fun getAllMulticart(requireContext: Context): LiveData<List<Int>?>?{
        multicartListFromDb = CartRespository.getallMulticartlist(requireContext)
        return multicartListFromDb
    }

    fun getMulticartById(requireContext: Context, status: String): LiveData<List<Int>?>?{
        multicartListByIdFromDb = CartRespository.getallMulticartlistById(requireContext,status)
        return multicartListByIdFromDb
    }

    fun getAllMulticartdetails(requireContext: Context, cart_id: Int): LiveData<List<MulticartTable>?>?{
        multicartdetailsByIdFromDb = CartRespository.getallMulticartlist(requireContext,cart_id)
        return multicartdetailsByIdFromDb
    }

    suspend fun addMulticartItems(
        context: Context,
        newcartlist: List<CartItemTable>,
        newcount: Int,
        Status: String,
        date: String
    ) {
        CartRespository.addMulticartItem(context, newcartlist,newcount,Status,date,"","")
    }

    fun removerFrommultiCart(requireContext: Context, multicart_id: Int?) {
        CartRespository.database!!.getDao().multicartdeleteById(multicart_id!!)
    }
}