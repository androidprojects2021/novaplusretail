package com.novaplus.retailer.view.activity

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.model.response.data.ResponseGetData
import com.novaplus.retailer.App
import com.novaplus.retailer.database.repository.CartRespository
import com.novaplus.retailer.database.repository.ProductRepository
import com.novaplus.retailer.database.tables.ItemTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import java.lang.Exception

@InternalCoroutinesApi
class BottomHostViewModel : ViewModel() {

    var allproductlistFromDb: List<ItemTable>? = null

    private val _getDataStatus = MutableLiveData<ResponseGetData>()
    val getDataStatus: LiveData<ResponseGetData>
        get() = _getDataStatus

    var bmulticartcountFromDb: LiveData<List<Int>?>? = null

    suspend fun getAllData(context: Context) {


        Log.d("Api Call - ViewModel", "getData: ")
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = App().retrofit.getData()

                Log.d("ViewModel", "getData: " + response)

                if (response.status!!.equals("success")) {
                    insertToDb(context, response)
                    _getDataStatus.value = response
                } else {
                    _getDataStatus.value = response

                }

            } catch (e: Exception) {
                _getDataStatus.value =
                    ResponseGetData(message = "Something went wrong", status = "error")
            }
        }


    }


    private suspend fun insertToDb(context: Context, response: ResponseGetData) {
        ProductRepository.insertData(context, response.categories, response.products)
    }


    suspend fun clearallTables(bottomHostActivity: BottomHostActivity) {
        ProductRepository.clearallTables(bottomHostActivity)
    }

    fun getmulticartcount(bottomHostActivity: BottomHostActivity): LiveData<List<Int>?>? {
        bmulticartcountFromDb = CartRespository.getmulticartcount(bottomHostActivity,"OnHold")
        return bmulticartcountFromDb
    }



    fun getAllproducts(
        bottomHostActivity: BottomHostActivity
    ): List<ItemTable>? {
        allproductlistFromDb =
            ProductRepository.getallItemsdetail(bottomHostActivity)
        return allproductlistFromDb
    }

    suspend fun clearCart(bottomHostActivity: BottomHostActivity) {
        CartRespository.clearCart(bottomHostActivity)
    }
}