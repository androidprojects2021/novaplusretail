package com.novaplus.retailer.view.fragments.customer

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.commons.Loader
import com.azinova.model.input.InputCreateCustomer
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.utils.ActivityUtils
import com.novaplus.retailer.view.adapter.CustomerAdapter
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_bottom_host.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.create_customer.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CreateCustomerFragment : Fragment() {
    private lateinit var customerviewModel: CretaeCustomerViewModel
    lateinit var customerAdapter: CustomerAdapter

    var getallcustomers= ArrayList<CustomersItem>()
    var SearchList = ArrayList<CustomersItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.create_customer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.ll_menu?.visibility = View.GONE
        activity?.ll_search?.visibility = View.INVISIBLE

      //  customerviewModel = ViewModelProvider(this).get(CretaeCustomerViewModel::class.java)



        onClick()
        onObserve()


    }

    private fun onObserve() {

    }

    private fun onClick() {
        ll_createcustomerback.setOnClickListener {
            val customerFragment: Fragment = CustomerFragment()
            ActivityUtils.replaceFragment(
                parentFragmentManager,
                customerFragment,
                R.id.main_content
            )
        }
    }

    /*@DelicateCoroutinesApi
    private fun init() {

        rv_customerlist.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        )
        GlobalScope.launch(Dispatchers.Main) {

        }

    }



    private fun onClick() {

        ed_customersearch.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(editable: Editable?) {



                SearchList.clear()
                if (editable.toString().equals("")) {
                    customerAdapter.refreshData(getallcustomers!!)
                } else {

                    if (getallcustomers?.size!! > 0) {
                        for (item in getallcustomers!!) {
                            if (item.name!!.contains(editable.toString(), ignoreCase = true)) {
                                SearchList.add(item!!)
                            }
                        }
                        customerAdapter.refreshData(SearchList!!)
                    }


                }



            }

        })
        addCustomer.setOnClickListener {
            isValid()
        }
    }

    private fun isValid() {
        if (customerName.text.isEmpty()) {
            Toasty.warning(requireContext(), "Please enter customer name", Toast.LENGTH_SHORT)
                .show()
        } else if (customerMobile.text.isEmpty()) {
            Toasty.warning(requireContext(), "Please enter customer mobile", Toast.LENGTH_SHORT)
                .show()
        } else if (customerPhone.text.isEmpty()) {
            Toasty.warning(requireContext(), "Please enter customer phone", Toast.LENGTH_SHORT)
                .show()
        } else if (customeremail.text.isEmpty()) {
            Toasty.warning(requireContext(), "Please enter customer email", Toast.LENGTH_SHORT)
                .show()
        } else {
            if (requireContext().isConnectedToNetwork()) {
                Loader.showLoader(requireContext())
                customerviewModel.addcustomer(
                    InputCreateCustomer(
                        customerName.text.toString(),
                        customerMobile.text.toString(),
                        customerPhone.text.toString(),
                        customeremail.text.toString(),
                        App().sharedPrefs.user_id?.toInt()
                    )
                )
            } else {
                Toast.makeText(requireContext(), "Please connect to internet", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun onObserve() {


        customerviewModel.createCustomerstatus.observe(viewLifecycleOwner, Observer {createcustomer ->
            Loader.hideLoader()
            if (createcustomer.result?.status.equals("success")) {
                Toast.makeText(requireContext(), createcustomer.result?.message, Toast.LENGTH_LONG).show()
                customerName.text.clear()
                customerMobile.text.clear()
                customerPhone.text.clear()
                customeremail.text.clear()
            } else {
                Toast.makeText(requireContext(), createcustomer.result?.message, Toast.LENGTH_LONG).show()

            }
        })
    }*/

    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}