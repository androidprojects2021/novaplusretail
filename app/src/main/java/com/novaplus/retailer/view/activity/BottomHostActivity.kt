package com.novaplus.retailer.view.activity

import android.annotation.SuppressLint
import android.content.*
import android.content.pm.ActivityInfo
import android.graphics.pdf.PdfDocument
import android.net.ConnectivityManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.azinova.commons.Loader
import com.google.android.material.badge.BadgeDrawable
import com.novaplus.retailer.App
import com.novaplus.retailer.BuildConfig
import com.novaplus.retailer.R
import com.novaplus.retailer.database.NovaplusDatabase
import com.novaplus.retailer.utils.ActivityUtils
import com.novaplus.retailer.view.fragments.carts.CartFragment
import com.novaplus.retailer.view.fragments.casher.CasherFragment
import com.novaplus.retailer.view.fragments.customer.CreateCustomerFragment
import com.novaplus.retailer.view.fragments.dashboard.DashboardFragment
import com.novaplus.retailer.view.fragments.product.ProductFragment
import com.novaplus.retailer.view.fragments.sales.SalesFragment
import com.novaplus.retailer.view.fragments.scanner.ScannerFragment
import com.novaplus.retailer.view.fragments.settings.SettingsFragment
import com.novaplus.retailer.view.splash.SplashScreenActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_bottom_host.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.coroutines.*
import com.google.android.gms.vision.barcode.Barcode
import com.novaplus.retailer.view.fragments.customer.CustomerFragment
import com.novaplus.retailer.view.fragments.payment.PaymentFragment
import java.lang.Math.log


@InternalCoroutinesApi
class BottomHostActivity : AppCompatActivity() {
    var database: NovaplusDatabase? = null
    var barcode = ""

    lateinit var bottomHostViewModel: BottomHostViewModel
    private val prefs = App().sharedPrefs
    private var wifireciever: BroadcastReceiver? = null
    private val numberOfLevels = 5

    private lateinit var navController: NavController
    private var badge: BadgeDrawable? = null

    var page = Pages.DASHBOARD

    enum class Pages {
        DASHBOARD,
        SALES,
        CUSTOMER,
        CASHIER,
        PRODUCT,
        SETTINGS,
        MULTIORDERCART,
        PAYMENT
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_host)
        setSupportActionBar(findViewById(R.id.toolbar))
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        bottomHostViewModel = ViewModelProvider(this).get(BottomHostViewModel::class.java)

        if (intent != null) {
            page = intent.getSerializableExtra("fragmentName") as Pages
            loadPage(page)
        }


        observe()

        init()

        onClick()
        DrawableClick()


    }

    private fun DrawableClick() {
        ll_clearitems.setOnClickListener {
            GlobalScope.launch {
                bottomHostViewModel.clearCart(this@BottomHostActivity)
                side_drawer.closeDrawer(GravityCompat.END)
            }
        }

        ll_autoinvoice.setOnClickListener {
            side_drawer.closeDrawer(GravityCompat.END)
        }

        ll_printreciept.setOnClickListener {
            side_drawer.closeDrawer(GravityCompat.END)
        }
        ll_assignedseller.setOnClickListener {
            side_drawer.closeDrawer(GravityCompat.END)
        }
        ll_recommendation.setOnClickListener {
            side_drawer.closeDrawer(GravityCompat.END)
        }
        ll_startcategory.setOnClickListener {
            side_drawer.closeDrawer(GravityCompat.END)
        }
        ll_sync.setOnClickListener {
            GlobalScope.launch() {
                if (this@BottomHostActivity.isConnectedToNetwork()) {
                    Loader.showLoader(this@BottomHostActivity)

                    bottomHostViewModel.getAllData(applicationContext)
                } else {
                    Toast.makeText(
                        this@BottomHostActivity,
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            side_drawer.closeDrawer(GravityCompat.END)
        }
        ll_deselectcustomer.setOnClickListener {

        }

    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        try {

            if (keyCode == KeyEvent.KEYCODE_BACK) {
                Log.e("back key pressed", "Back key pressed" + keyCode)
                onexit()
                barcodeLookup(keyCode.toString())
                return true
            }
        } catch (e: Exception) {
        }
        return super.onKeyDown(keyCode, event)

    }


    private fun onexit() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            Log.d("TAG", "onexit: "+ supportFragmentManager.fragments)

            supportFragmentManager.popBackStack()
    //      super.onBackPressed()
        } else {

            val alertDialog = AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure, Want to Exit!")

                .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    finish()
                })
                .setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })
            val alert = alertDialog.create()
            alert.show()

        }
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (event?.getAction() == KeyEvent.KEYCODE_BACK) {
            Toast.makeText(this, "Backpressed", Toast.LENGTH_LONG).show()
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        if (event != null) {
            if (event?.getAction() == KeyEvent.KEYCODE_BACK) {
                Toast.makeText(this, "Backpressed  1", Toast.LENGTH_LONG).show()
            }

            if (event.getAction() === KeyEvent.ACTION_DOWN
                && event.getKeyCode() !== KeyEvent.KEYCODE_ENTER
            ) {

            }
        }

        try {


            if (event != null) {
                if (event.getAction() === KeyEvent.ACTION_DOWN
                    && event.getKeyCode() === KeyEvent.KEYCODE_ENTER
                ) {
                    Log.i("TAG", "Barcode Read: $barcode")
                    Toast.makeText(this, "Barcode Result" + barcode, Toast.LENGTH_LONG).show()
                    // barcodeLookup(barcode) // or Any method handling the data
                    // barcode = ""
                }
            }
        } catch (e: Exception) {
        }

        return super.dispatchKeyEvent(event)
    }

    private fun barcodeLookup(barcode: String) {
        try {

            val allproductlistFromDb = bottomHostViewModel.getAllproducts(this)
            if (!allproductlistFromDb.isNullOrEmpty()) {
                for (item in allproductlistFromDb) {
                    if (item.barcode.equals(barcode)) {
                        if (item.qty_available!! > 0) {
                            /* bottomHostViewModel.addToCartItem(this
                             ,item
                             )*/
                        }
                    } else {
                        Toasty.warning(this, "Item not found", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        } catch (e: Exception) {
        }
    }


    private fun init() {

        if (checkLoggedIn()) {
            txt_username.text = prefs.user_name
        }
        GlobalScope.launch(Dispatchers.Main) {

            bottomHostViewModel.getmulticartcount(this@BottomHostActivity)
                ?.observe(this@BottomHostActivity,
                    Observer { multicartcountArray ->

                        if (multicartcountArray != null) {

                            if (multicartcountArray.size > 0) {
                                txt_multicartcount.visibility = View.VISIBLE
                                txt_multicartcount.text = multicartcountArray.size.toString()
                            } else {
                                txt_multicartcount.visibility = View.GONE
                            }
                        }
                    })


        }

    }


    private fun onClick() {

        ll_pos.setOnClickListener {
            page = Pages.DASHBOARD
            loadPage(page)
        }
        ll_sales.setOnClickListener {
            page = Pages.SALES
            loadPage(page)
        }
        ll_customer.setOnClickListener {
            page = Pages.CUSTOMER
            loadPage(page)
        }
        ll_casher.setOnClickListener {
            page = Pages.CASHIER
            loadPage(page)
        }
        ll_product.setOnClickListener {
            page = Pages.PRODUCT
            loadPage(page)
        }
        ll_settings.setOnClickListener {
            page = Pages.SETTINGS
            loadPage(page)
        }
        img_multicart.setOnClickListener {
            page = Pages.MULTIORDERCART
            loadPage(page)
        }


        btn_logout.setOnClickListener(View.OnClickListener {
            android.app.AlertDialog.Builder(this)
                .setMessage("Are you sure.")
                .setTitle("Logout !").setPositiveButton(
                    "Yes"
                ) { dialog: DialogInterface?, which: Int ->
                    prefs.clearSession(this)
                    database?.clearAllTables()
                    GlobalScope.launch() {
                        bottomHostViewModel.clearallTables(this@BottomHostActivity)
                    }
                    startActivity(Intent(this, SplashScreenActivity::class.java))
                    finish()
                }.setNegativeButton("No ", null).show()
        })


    }


    private fun loadPage(pages: Pages) {
        when (pages) {
            Pages.DASHBOARD -> {
                ll_pos.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_light))
                ll_sales.setBackgroundResource(R.drawable.menu_bg)
                ll_customer.setBackgroundResource(R.drawable.menu_bg)
                ll_casher.setBackgroundResource(R.drawable.menu_bg)
                ll_product.setBackgroundResource(R.drawable.menu_bg)
                ll_settings.setBackgroundResource(R.drawable.menu_bg)


                val dashboardFragment: Fragment = DashboardFragment()
                ActivityUtils.replaceFragment(
                    supportFragmentManager,
                    dashboardFragment,
                    R.id.main_content
                )
                ll_menu.visibility = View.VISIBLE
                ll_search.visibility = View.VISIBLE
            }

            Pages.SALES -> {
                ll_pos.setBackgroundResource(R.drawable.menu_bg)
                ll_sales.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_light))
                ll_customer.setBackgroundResource(R.drawable.menu_bg)
                ll_casher.setBackgroundResource(R.drawable.menu_bg)
                ll_product.setBackgroundResource(R.drawable.menu_bg)
                ll_settings.setBackgroundResource(R.drawable.menu_bg)

                val salesFragment: Fragment = SalesFragment()
                ActivityUtils.replaceFragment(
                    supportFragmentManager,
                    salesFragment,
                    R.id.main_content
                )
                ll_menu.visibility = View.VISIBLE
                ll_search.visibility = View.INVISIBLE
            }
            Pages.CUSTOMER -> {
                ll_pos.setBackgroundResource(R.drawable.menu_bg)
                ll_sales.setBackgroundResource(R.drawable.menu_bg)
                ll_customer.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_light))
                ll_casher.setBackgroundResource(R.drawable.menu_bg)
                ll_product.setBackgroundResource(R.drawable.menu_bg)
                ll_settings.setBackgroundResource(R.drawable.menu_bg)

                val customerFragment: Fragment = CustomerFragment()
                ActivityUtils.replaceFragment(
                    supportFragmentManager,
                    customerFragment,
                    R.id.main_content
                )
                ll_menu.visibility = View.VISIBLE
                ll_search.visibility = View.INVISIBLE
            }

            Pages.CASHIER -> {
                ll_pos.setBackgroundResource(R.drawable.menu_bg)
                ll_sales.setBackgroundResource(R.drawable.menu_bg)
                ll_customer.setBackgroundResource(R.drawable.menu_bg)
                ll_casher.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_light))
                ll_product.setBackgroundResource(R.drawable.menu_bg)
                ll_settings.setBackgroundResource(R.drawable.menu_bg)

                val casherFragment: Fragment = CasherFragment()
                ActivityUtils.replaceFragment(
                    supportFragmentManager,
                    casherFragment,
                    R.id.main_content
                )
                ll_menu.visibility = View.VISIBLE
                ll_search.visibility = View.INVISIBLE
            }
            Pages.PRODUCT -> {
                ll_pos.setBackgroundResource(R.drawable.menu_bg)
                ll_sales.setBackgroundResource(R.drawable.menu_bg)
                ll_customer.setBackgroundResource(R.drawable.menu_bg)
                ll_casher.setBackgroundResource(R.drawable.menu_bg)
                ll_product.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_light))
                ll_settings.setBackgroundResource(R.drawable.menu_bg)

                val productFragment: Fragment = ProductFragment()
                ActivityUtils.replaceFragment(
                    supportFragmentManager,
                    productFragment,
                    R.id.main_content
                )
                ll_menu.visibility = View.VISIBLE
                ll_search.visibility = View.INVISIBLE
            }
            Pages.SETTINGS -> {
                ll_settings.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_light))

                ll_pos.setBackgroundResource(R.drawable.menu_bg)
                ll_sales.setBackgroundResource(R.drawable.menu_bg)
                ll_customer.setBackgroundResource(R.drawable.menu_bg)
                ll_casher.setBackgroundResource(R.drawable.menu_bg)
                ll_product.setBackgroundResource(R.drawable.menu_bg)

                val settingsFragment: Fragment = SettingsFragment()
                ActivityUtils.replaceFragment(
                    supportFragmentManager,
                    settingsFragment,
                    R.id.main_content
                )
                ll_menu.visibility = View.VISIBLE
                ll_search.visibility = View.INVISIBLE
            }
            Pages.MULTIORDERCART -> {

                val cartFragment: Fragment = CartFragment()
                ActivityUtils.addFragment(
                    supportFragmentManager,
                    cartFragment,
                    R.id.main_content
                )



                ll_menu.visibility = View.GONE
                ll_search.visibility = View.INVISIBLE
            }

            else -> {
                ll_search.visibility = View.INVISIBLE
                ll_menu.visibility = View.GONE
            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    @SuppressLint("WifiManagerLeak")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            R.id.txt_newest -> {
                side_drawer.openDrawer(GravityCompat.END)
                return true
            }
            /* R.id.notification -> {
                 Toasty.warning(applicationContext, "Coming Soon", Toast.LENGTH_SHORT).show()
                 return true
             }*/
            R.id.wifi -> {
//                initializeWiFiListener()
                val wifi = getSystemService(WIFI_SERVICE) as WifiManager
                wifi.isWifiEnabled = true
                return true
            }
            R.id.sync -> {
                GlobalScope.launch() {
                    if (this@BottomHostActivity.isConnectedToNetwork()) {
                        Loader.showLoader(this@BottomHostActivity)

                        bottomHostViewModel.getAllData(applicationContext)
                    } else {
                        Toast.makeText(
                            this@BottomHostActivity,
                            "Please connect to internet",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    private fun observe() {
        bottomHostViewModel.getDataStatus.observe(this, Observer { itemdatas ->
            Loader.hideLoader()
            if (itemdatas.status!!.equals("success")) {
                Log.d("Categories ", "GetData: " + itemdatas.categories)
                Log.d("Products", "GetData: " + itemdatas.products)

                Toasty.success(this, "Updated...", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, itemdatas.message, Toast.LENGTH_LONG).show()

            }
        })
    }


    override fun onBackPressed() {

        onexit()
        /* if (supportFragmentManager.backStackEntryCount > 0) {
             supportFragmentManager.popBackStack()
           //  super.onBackPressed()
         } else {

             val alertDialog = AlertDialog.Builder(this)
                 .setTitle("Exit")
                 .setMessage("Are you sure, Want to Exit!")

                 .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                     dialog.dismiss()
                     finish()
                 })
                 .setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                     dialog.dismiss()
                 })
             val alert = alertDialog.create()
             alert.show()

         }*/


    }


    public fun notificationBadge(cartQty: Int) {

        runOnUiThread {
            if (cartQty == 0) {
                badge?.isVisible = false
            } else {
                badge?.isVisible = true
                badge?.number = cartQty
            }

        }

    }

    private lateinit var customLayout: View


    private fun onUpdateNeeded(isMandatoryUpdate: Boolean) {
        val dialogBuilder = AlertDialog.Builder(this)
            .setTitle("New Update")
            .setCancelable(false)
            .setMessage(if (isMandatoryUpdate) "Please update Nova+ Retail application to continue.." else "A new version is found on Play store, please update for better usage.")
            .setPositiveButton("update")
            { dialog, which ->
                openAppOnPlayStore(this, BuildConfig.APPLICATION_ID)
            }

        if (!isMandatoryUpdate) {
            dialogBuilder.setNegativeButton("later") { dialog, which ->
                dialog?.dismiss()
            }.create()
        }
        val dialog: AlertDialog = dialogBuilder.create()
        dialog.show()
    }

    private fun openAppOnPlayStore(ctx: BottomHostActivity, package_name: String?) {
        var package_name = package_name
        if (package_name == null) {
            package_name = ctx.packageName
        }
        val uri = Uri.parse("market://details?id=$package_name")
        openURI(ctx, uri, "Play Store not found in your device")
    }


    fun openURI(
        ctx: Context,
        uri: Uri?,
        error_msg: String?
    ) {
        val i = Intent(Intent.ACTION_VIEW, uri)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (ctx.packageManager.queryIntentActivities(i, 0).size > 0) {
            ctx.startActivity(i)
        } else if (error_msg != null) {
            Toast.makeText(this, error_msg, Toast.LENGTH_SHORT).show()
        }
    }


    override fun onResume() {
        super.onResume()
    }

    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

    fun checkLoggedIn(): Boolean {
        return App().sharedPrefs.user_name != null
    }


    /*    private fun initializeWiFiListener() {

        val connectivity_context = WIFI_SERVICE
        val wifi = getSystemService(connectivity_context) as WifiManager
        if (!wifi.isWifiEnabled) {
            if (wifi.wifiState != WifiManager.WIFI_STATE_ENABLING) {
                wifi.isWifiEnabled = true
            }
        }

        wifireciever = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val info = wifi.connectionInfo
                TODO: implement methods for action handling
                val level = WifiManager.calculateSignalLevel(info.rssi, numberOfLevels)
                if (level == 1) {
                    wifi.setImageResource(R.drawable.wifi0)
                } else if (level == 2) {
                    wifi.setImageResource(R.drawable.wifi1)
                } else if (level == 3) {
                    wifi.setImageResource(R.drawable.wifi2)
                } else if (level == 4) {
                    wifi.setImageResource(R.drawable.wifi3)
                } else if (level == 5) {
                    wifi.setImageResource(R.drawable.ic_wifi_icon)
                }
            }
        }

        registerReceiver(wifireciever, IntentFilter(WifiManager.RSSI_CHANGED_ACTION))
    }

    override fun onDestroy() {
        try {
            if (wifireciever != null) unregisterReceiver(wifireciever)
        } catch (e: Exception) {
            Log.e("exp", "receiver exception ::  $e")
        }
        super.onDestroy()
        disposables.clear() // do not send event after activity has been destroyed
    }*/

/*     private void showAlert(String message, String from) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View view = LayoutInflater.from(this).inflate(R.layout.custome_dialogue, null);
            TextView txtMessage = view.findViewById(R.id.txtMessage);

            Button btnCancel = view.findViewById(R.id.btnCancel);


            TextView btnShow = view.findViewById(R.id.btnShow);

            txtMessage.setText(message);

            builder.setView(view);

            final AlertDialog alertDialog = builder.create();

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    alertDialog.dismiss();
                }
            });

            btnShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("fragmentName", DetailActivity.Pages.ORDER);
                    intent.putExtra("from", FCM_PUSH);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);



                    alertDialog.dismiss();
                }
            });

            alertDialog.show();

        }*/
}