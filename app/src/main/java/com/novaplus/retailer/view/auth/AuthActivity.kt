package com.novaplus.retailer.view.auth

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.novaplus.retailer.R
class AuthActivity : AppCompatActivity() {
    private lateinit var navControl: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        navControl = findNavController(R.id.fragment2)
    }

    override fun onBackPressed() {
        exitPopUp()
    }
    private fun exitPopUp() {

        if (navControl.currentDestination?.id == R.id.loginFragment) {
            val alertDialog = AlertDialog.Builder(this)
                .setTitle("Exit")
                .setMessage("Are you sure, Want to Exit!")
//                .setIcon(R.drawable.ic_exit_to_app)
                .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    finish()
                })
                .setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })
            val alert = alertDialog.create()
            alert.show()

        } else {
            super.onBackPressed()
        }
    }



}