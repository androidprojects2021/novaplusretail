package com.novaplus.retailer.view.fragments.scanner

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScanner.ALL_FORMATS
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.google.android.gms.common.util.CollectionUtils
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.barcode.Barcode.ALL_FORMATS
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.view.adapter.ProductListAdapter
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_scanner.*
import kotlinx.coroutines.InternalCoroutinesApi
import org.json.JSONException

@InternalCoroutinesApi
class ScannerFragment : DialogFragment() {

    private lateinit var scannerViewModel: ScannerViewModel
    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private val REQUEST_CAMERA_PERMISSION = 201
    private var intentData = ""
    var REQUESTCODE = 111

    private lateinit var codeScanner: CodeScanner

    var allproductlist: List<ItemTable>? = ArrayList()
    var productaddtocart: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {


            }
        })


        arguments?.let {
        }


    }

    fun init() {
        scannerViewModel.getAllproducts(requireContext())
            ?.observe(
                viewLifecycleOwner,
                Observer { allproductlistFromDb ->


                    allproductlist = allproductlistFromDb

                    Log.d(
                        "TAG",
                        "product check: " + allproductlist
                    )

                })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scannerViewModel = ViewModelProvider(this).get(ScannerViewModel::class.java)
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_CAMERA_PERMISSION
            )
        } else {
            startScanning()
        }
        init()
    }

    private fun startScanning() {
        // Parameters (default values)
        codeScanner = CodeScanner(requireContext(), scanner_view)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS
        //CollectionUtils.listOf(BarcodeFormat.AZTEC) //.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.CONTINUOUS // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {

            activity?.runOnUiThread {
                Toast.makeText(requireContext(), "Scan result:-  ${it.text}", Toast.LENGTH_LONG)
                    .show()
                Log.d("Scan result:- ", "startScanning: " + it.text)
                copyToClipBoard(it.text)

            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            activity?.runOnUiThread {
                Toast.makeText(
                    requireContext(), "Camera initialization error:-  ${it.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        scanner_view.setOnClickListener {
            codeScanner.startPreview()
        }
    }


    private fun copyToClipBoard(text: String?) {
        if (!text.isNullOrEmpty()) {
            if (allproductlist?.size!! > 0) {
                for (item in allproductlist!!) {
                    if (item.barcode.equals(text)) {
                        productaddtocart = item.barcode
                    }

                }
                if (productaddtocart.isNullOrEmpty()){
                    Toast.makeText(requireContext(), "Product not found", Toast.LENGTH_LONG)
                        .show()
                }else {

                    Toast.makeText(
                        requireContext(),
                        "Product found Successfully",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (::codeScanner.isInitialized) {
            codeScanner?.startPreview()
        }
    }

    override fun onPause() {
        super.onPause()
        if (::codeScanner.isInitialized) {
            codeScanner?.releaseResources()
        }
    }

    companion object {

        fun newInstance() =
            ScannerFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}