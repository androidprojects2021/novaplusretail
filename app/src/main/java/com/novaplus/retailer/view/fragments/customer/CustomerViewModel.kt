package com.novaplus.retailer.view.fragments.customer

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.model.response.customers_list.CustomerListResponse
import com.novaplus.retailer.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class CustomerViewModel:ViewModel() {

    private val _getAllCustomers = MutableLiveData<CustomerListResponse>()
    val getAllCustomers: LiveData<CustomerListResponse>
        get() = _getAllCustomers

    fun getAllCustomers() {
        Log.d("Api Call - ViewModel", "getCustomers: ")
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = App().retrofit.getCustomerList()

                Log.d("ViewModel", "getCustomers: " + response)

                if (response.status.equals("success")) {
                    _getAllCustomers.value = response
                } else {
                    _getAllCustomers.value = response
                }

            } catch (e: Exception) {
                Log.d("Exception", "getAllCustomers: ")
                _getAllCustomers.value =
                    CustomerListResponse(message = "Something went wrong", status = "error")
            }
        }
    }
}