package com.novaplus.retailer.view.fragments.carts

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.MulticartTable
import com.novaplus.retailer.utils.ActivityUtils
import com.novaplus.retailer.view.adapter.MulticartAdapter
import com.novaplus.retailer.view.adapter.SpinnerAdapter
import com.novaplus.retailer.view.fragments.dashboard.DashboardFragment
import com.novaplus.retailer.view.fragments.payment.PaymentFragment
import kotlinx.android.synthetic.main.fragment_cartlist.*
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@DelicateCoroutinesApi
@InternalCoroutinesApi
class CartFragment : Fragment(), MulticartAdapter.MultiCartInterface {
    private lateinit var cartViewModel: CartViewModel

    private var multicartAdapter: MulticartAdapter? = null


    var newmulticartListFromDb: ArrayList<MulticartTable>? = ArrayList()

    lateinit var spinner: Spinner
    lateinit var adapter: SpinnerAdapter

    val statusList = arrayOf("All Orders", "OnHold", "Finished")// "Ongoing"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                     requireActivity().onBackPressed()
               /* val dashboardFragment: Fragment = DashboardFragment()
                ActivityUtils.replaceFragmentAddtoBackStack(
                    parentFragmentManager,
                    dashboardFragment,
                    R.id.main_content
                )
*/
            }
        })


    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_cartlist, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cartViewModel = ViewModelProvider(this).get(CartViewModel::class.java)
        onViewset()
        spinnerfun(view)

        searchtext.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(stringdata: Editable?) {
                Log.d("TAG", "afterTextChanged: " + stringdata)
            }
        })

        neworderButton.setOnClickListener {

            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val currentDate = sdf.format(Date())

            Log.d("CARTCOUNT", "onViewCreated: " + App().sharedPrefs.cartcount)
            GlobalScope.launch {


                val cartlist = cartViewModel.getAllCartitems(requireContext())

                if (cartlist != null) {
                    if (cartlist.size > 0) {
                        val newcount = App().sharedPrefs.cartcount.plus(1)
                        App().sharedPrefs.cartcount = newcount
                        Log.d("NewCartCount ", "onViewCreated: " + newcount)


                        cartViewModel.addMulticartItems(
                            requireContext(),
                            cartlist,
                            newcount,
                            "OnHold",
                            currentDate
                        )

                    } else {
                        Log.d(
                            "NewCartCount 1",
                            "onViewCreated: " + App().sharedPrefs.cartcount
                        )
                    }
                }
                val dashboardFragment: Fragment = DashboardFragment()
                ActivityUtils.replaceFragment(
                    parentFragmentManager,
                    dashboardFragment,
                    R.id.main_content
                )


            }


        }

        img_close.setOnClickListener {

           // requireActivity().onBackPressed()
            val dashboardFragment: Fragment = DashboardFragment()
            ActivityUtils.replaceFragmentAddtoBackStack(
                parentFragmentManager,
                dashboardFragment,
                R.id.main_content
            )
        }
    }

    @DelicateCoroutinesApi
    private fun spinnerfun(view: View) {


        adapter = SpinnerAdapter(requireContext(), statusList)
        spinner = view.findViewById(R.id.sp_status) as Spinner
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

                Log.d("Spinner", "onItemSelected: " + statusList.get(position))

                when (statusList.get(position)) {
                    "All Orders" -> {
                        getallOrders()
                    }
                    "OnHold" -> {
                        getordersByStatus("OnHold")
                    }
                    "Finished" -> {
                        getordersByStatus("Finished")
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

    }

    private fun getordersByStatus(status: String) {
        newmulticartListFromDb?.clear()

        GlobalScope.launch(Dispatchers.Main) {
            cartViewModel.getMulticartById(requireContext(), status)?.observe(viewLifecycleOwner,
                androidx.lifecycle.Observer { multicartlist ->

                    Log.d("MULTICART Id statusList", "onViewset: " + multicartlist)
                    if (!multicartlist.isNullOrEmpty()) {
                        orderlist_rv.visibility = View.VISIBLE
                        newmulticartListFromDb?.clear()
                        multicartlist?.forEach {

                            GlobalScope.launch(Dispatchers.Main) {
                                cartViewModel.getAllMulticartdetails(requireContext(), it)
                                    ?.observe(viewLifecycleOwner,
                                        androidx.lifecycle.Observer { cartdetails ->

                                            Log.d("Test1 - status ", "getallOrders: " + cartdetails)

                                            if (!cartdetails.isNullOrEmpty()) {
                                                orderlist_rv.visibility = View.VISIBLE
                                                if (cartdetails.size > 0) {

                                                    newmulticartListFromDb?.add(cartdetails.get(0))


                                                }
                                            } else {
                                                orderlist_rv.visibility = View.GONE
                                            }


                                            multicartAdapter = MulticartAdapter(
                                                requireContext(),
                                                newmulticartListFromDb,
                                                this@CartFragment
                                            ) {
                                                val alertDialog =
                                                    AlertDialog.Builder(requireContext())
                                                        .setTitle("Remove Item From Cart")
                                                        .setMessage("Are you sure, Want to remove this item !")

                                                        .setPositiveButton(
                                                            "Yes",
                                                            DialogInterface.OnClickListener { dialog, which ->


                                                                GlobalScope.launch {
                                                                    cartViewModel.removerFrommultiCart(
                                                                        requireContext(),
                                                                        it?.multicart_id
                                                                    )
                                                                }


                                                                dialog.dismiss()
                                                                getallOrders()
                                                            })
                                                        .setNegativeButton(
                                                            "No",
                                                            DialogInterface.OnClickListener { dialog, which ->
                                                                dialog.dismiss()
                                                            })
                                                val alert = alertDialog.create()
                                                alert.show()
                                            }
                                            orderlist_rv.adapter = multicartAdapter


                                            /*  if (cartdetails != null) {
                                              if (!(cartdetails.size>0)){
                                                  newmulticartListFromDb?.clear()
                                              }
                                              for (item in cartdetails) {
                                                  newmulticartListFromDb?.add(item)

                                              }
                                              multicartAdapter?.refreshdata(newmulticartListFromDb)
                                             multicartAdapter?.notifyDataSetChanged()
                                             Log.d(
                                                 "MULTICART Details ",
                                                 "onViewset: " + newmulticartListFromDb
                                             )
                                         }*/

                                        })

                            }
                        }
                    } else {
                        orderlist_rv.visibility = View.GONE
                    }

                })
        }
    }

    @DelicateCoroutinesApi
    private fun getallOrders() {
        newmulticartListFromDb?.clear()
        GlobalScope.launch(Dispatchers.Main) {
            try {


                cartViewModel.getAllMulticart(requireContext())
                    ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer { multicartlist ->
                        Log.d("MULTICART Id allList", "onViewset: " + multicartlist)

                        if (!multicartlist.isNullOrEmpty()) {
                            orderlist_rv.visibility = View.VISIBLE
                            newmulticartListFromDb?.clear()
                            multicartlist?.forEach {

                                Log.d("MULTICART Id", "onViewset: " + it)

                                GlobalScope.launch(Dispatchers.Main) {

                                    cartViewModel.getAllMulticartdetails(requireContext(), it)
                                        ?.observe(viewLifecycleOwner,
                                            androidx.lifecycle.Observer { cartdetails ->

                                                Log.d(
                                                    "Test1 - all ",
                                                    "getallOrders: " + cartdetails
                                                )

                                                if (!cartdetails.isNullOrEmpty()) {
                                                    orderlist_rv.visibility = View.VISIBLE
                                                    if (cartdetails.size > 0) {


                                                        newmulticartListFromDb?.add(
                                                            cartdetails.get(
                                                                0
                                                            )
                                                        )


                                                    }


                                                } else {
                                                    orderlist_rv.visibility = View.GONE
                                                }


                                                multicartAdapter = MulticartAdapter(
                                                    requireContext(),
                                                    newmulticartListFromDb,
                                                    this@CartFragment
                                                ) {
                                                    val alertDialog =
                                                        AlertDialog.Builder(requireContext())
                                                            .setTitle("Remove Item From Cart")
                                                            .setMessage("Are you sure, Want to remove this item !")

                                                            .setPositiveButton(
                                                                "Yes",
                                                                DialogInterface.OnClickListener { dialog, which ->


                                                                    GlobalScope.launch {
                                                                        cartViewModel.removerFrommultiCart(
                                                                            requireContext(),
                                                                            it?.multicart_id
                                                                        )
                                                                    }


                                                                    dialog.dismiss()
                                                                    getallOrders()
                                                                })
                                                            .setNegativeButton(
                                                                "No",
                                                                DialogInterface.OnClickListener { dialog, which ->
                                                                    dialog.dismiss()
                                                                })
                                                    val alert = alertDialog.create()
                                                    alert.show()
                                                }
                                                orderlist_rv.adapter = multicartAdapter


                                            })

                                }


                            }
                            Log.d(
                                "Test2 - all Final array ",
                                "getallOrders: " + newmulticartListFromDb
                            )
                        } else {
                            orderlist_rv.visibility = View.GONE
                        }
                    })

            } catch (e: Exception) {
                Log.d("Exception", "getallOrders: " + e.localizedMessage)
            }
        }
    }

    private fun onViewset() {

        orderlist_rv.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        )

        Log.d("TAG - initial", "onViewset: " + newmulticartListFromDb)
        /* multicartAdapter = MulticartAdapter(requireContext(), newmulticartListFromDb,this) {


            */
        /* val alertDialog = AlertDialog.Builder(requireContext())
                    .setTitle("Remove Item From Cart")
                    .setMessage("Are you sure, Want to remove this item !")

                    .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->


                        GlobalScope.launch {
                            cartViewModel.removerFrommultiCart(
                                requireContext(),
                                it?.multicart_id
                            )
                        }


                        dialog.dismiss()
                    })
                    .setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                        dialog.dismiss()
                    })
                val alert = alertDialog.create()
                alert.show()*/
        /*


            }
            orderlist_rv.adapter = multicartAdapter
    */


    }

    companion object {
        fun newInstance() =
            CartFragment().apply {
                arguments = Bundle().apply {

                }
            }

    }

    override fun clickItem(getitem: MulticartTable?) {
        Log.d("TAG", "clickItem: " + getitem)

        val paymentFragment: Fragment = PaymentFragment()

        val model_bundle = Bundle()
        model_bundle.putString("from", "CartFragment")
        model_bundle.putString("Total Amount", getitem?.total.toString())

        paymentFragment.arguments = model_bundle
        ActivityUtils.addFragment(
            parentFragmentManager,
            paymentFragment,
            R.id.main_content
        )
    }

    override fun receiptItemClick(selecteditem: MulticartTable?) {
        Log.d("Receipt Order", "receiptItemClick: "+selecteditem)

    }




}