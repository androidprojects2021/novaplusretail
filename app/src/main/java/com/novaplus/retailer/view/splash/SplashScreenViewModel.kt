package com.novaplus.retailer.view.splash

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.azinova.model.response.data.ResponseGetData
import com.novaplus.retailer.App
import com.novaplus.retailer.database.repository.ProductRepository
import com.novaplus.retailer.database.tables.ItemTable
import kotlinx.coroutines.*
import java.lang.Exception


@InternalCoroutinesApi
class SplashScreenViewModel : ViewModel() {


    private val _getDataStatus = MutableLiveData<ResponseGetData>()
    val getDataStatus: LiveData<ResponseGetData>
        get() = _getDataStatus

   // var itemlistFromDb: List<ItemTable>? = null
    var itemlistFromDb: LiveData<List<ItemTable>?>? = null



     fun getAllData(context: Context) {


        Log.d("Api Call - ViewModel", "getData: ")
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = App().retrofit.getData()

                Log.d("ViewModel", "getData: " + response)

                if (response.status!!.equals("success")) {
                    insertToDb(context, response)
                    _getDataStatus.value = response
                } else {
                    _getDataStatus.value = response

                }

            } catch (e: Exception) {
                _getDataStatus.value =
                    ResponseGetData(message = "Something went wrong", status = "error")
            }
        }


    }

    @InternalCoroutinesApi
    private suspend fun insertToDb(context: Context, response: ResponseGetData) {
        ProductRepository.insertData(context, response.categories, response.products)
    }

   /*suspend fun getItemsdetails(context: Context): List<ItemTable>? {
        itemlistFromDb = ProductRepository.getItemsdetails(context)
        return itemlistFromDb
    }*/

     fun getItemsdetails(context: Context): LiveData<List<ItemTable>?>? {
        itemlistFromDb = ProductRepository.getItemsdetails(context)
        return itemlistFromDb
    }


}
