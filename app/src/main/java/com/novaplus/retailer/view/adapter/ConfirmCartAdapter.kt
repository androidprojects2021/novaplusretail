package com.novaplus.retailer.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.CartItemTable
import kotlinx.android.synthetic.main.adapter_cart_recycler.view.*

class ConfirmCartAdapter(val context: Context, val cartlist: ArrayList<CartItemTable>?) :
    RecyclerView.Adapter<ConfirmCartAdapter.PlaceViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ConfirmCartAdapter.PlaceViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_confirm_cart, parent, false)
        return PlaceViewHolder(view)
    }

    override fun onBindViewHolder(holder: ConfirmCartAdapter.PlaceViewHolder, position: Int) {
        holder.item_name.text = cartlist?.get(position)?.item_name
        holder.quantity.text = cartlist?.get(position)?.cartQty.toString()
        holder.item_price.text = cartlist?.get(position)?.price.toString()

        val totalprice = (cartlist!![position]?.price!! * cartlist!![position]?.cartQty!!)

        val total3digits: Double = Math.round(totalprice!! * 1000.0) / 1000.0
        Log.d("total3digits", "onViewset: " + total3digits)
        val total2digits: Double = Math.round(total3digits * 100.0) / 100.0
        Log.d("total2digits", "onViewset: " + total2digits)

        holder.item_total.text = total2digits.toString()

    }

    override fun getItemCount(): Int {
        return cartlist!!.size
    }

    inner class PlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item_name = itemView.findViewById<TextView?>(R.id.item_name)
        val quantity = itemView.findViewById<TextView?>(R.id.quantity)
        val item_price = itemView.findViewById<TextView?>(R.id.item_price)
        val item_total = itemView.findViewById<TextView?>(R.id.item_total)
    }
}