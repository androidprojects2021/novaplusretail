package com.novaplus.retailer.view.auth.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.commons.Loader
import com.azinova.model.input.InputLogin
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.view.activity.BottomHostActivity
import com.novaplus.retailer.view.adapter.CustomerAdapter
import com.novaplus.retailer.views.auth.login.LoginViewModel
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.create_customer.*
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@InternalCoroutinesApi
class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private val prefs = App().sharedPrefs
    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        init()
        onClick()
        onObserve()


    }

    private fun onObserve() {
        viewModel.loginResponse.observe(viewLifecycleOwner, androidx.lifecycle.Observer {

            Loader.hideLoader()
            if (it.result?.status.equals("success")) {
                prefs.user_id = it.result!!.details!!.id.toString()
                prefs._name = it.result!!.details!!.name.toString()
                prefs.user_name = it.result!!.details!!.username

                prefs.login = true
                Toasty.success(requireContext(), it.result!!.message!!, Toast.LENGTH_SHORT)
                    .show()
                getData()
            } else {

                Toasty.error(
                    requireContext(),
                    it.result?.message.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
           /* when {

                it.result!!.status!!.equals("success") -> {

                    prefs.user_id = it.result!!.details!!.id.toString()
                    prefs.user_details = it.result!!.details!!.name.toString()

                    prefs.login = true
                    Toasty.success(requireContext(), it.result!!.message!!, Toast.LENGTH_SHORT)
                        .show()
                    getData()
                    *//* val intent = Intent(requireContext(), BottomHostActivity::class.java)
                      intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                      startActivity(intent)*//*

                }

                it.result!!.equals("failed") -> {
                    Toasty.error(
                        requireContext(),
                        "Invalid username or password. Please try again",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {

                }
            }*/
        })



        viewModel.getDataStatus.observe(viewLifecycleOwner, Observer { itemdatas ->
            Loader.hideLoader()
            if (itemdatas.status.equals("success")) {
                Log.d("Categories ", "GetData: " + itemdatas.categories)
                Log.d("Products", "GetData: " + itemdatas.products)
                /*val intent = Intent(requireContext(), BottomHostActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)*/
                val intent = Intent(requireContext(), BottomHostActivity::class.java)
                intent.putExtra("fragmentName", BottomHostActivity.Pages.DASHBOARD)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(requireContext(), itemdatas.message, Toast.LENGTH_LONG).show()
               /* val intent = Intent(requireContext(), BottomHostActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)*/
                val intent = Intent(requireContext(), BottomHostActivity::class.java)
                intent.putExtra("fragmentName", BottomHostActivity.Pages.DASHBOARD)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        })
    }


    fun getData() {
        GlobalScope.launch(Dispatchers.Main) {
            if (requireContext().isConnectedToNetwork()) {
                Loader.showLoader(requireContext())

                viewModel.getAllData(requireContext())


            } else {
                Toast.makeText(requireContext(), "Please connect to internet", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }


    private fun init() {

    }

    private fun onClick() {
        loginButton.setOnClickListener(View.OnClickListener {
            isValidate()
        })


    }

    private fun isValidate() {
        if (userNameInput.text.isEmpty()) {
            Toasty.warning(requireContext(), "Please enter username", Toast.LENGTH_SHORT).show()
        } else if (passwordInput.text.isEmpty()) {
            Toasty.warning(requireContext(), "Please enter password", Toast.LENGTH_SHORT)
                .show()
        } else {
            GlobalScope.launch (Dispatchers.Main){
                val inputLogin = InputLogin()
                inputLogin.username = userNameInput.text.toString()
                inputLogin.password = passwordInput.text.toString()

                if (requireContext().isConnectedToNetwork()) {
                    Loader.showLoader(requireContext())
                    viewModel.login(inputLogin)
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }


            }
        }
    }

    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}