package com.novaplus.retailer.view.splash

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.commons.Loader
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.view.activity.BottomHostActivity
import com.novaplus.retailer.view.auth.AuthActivity
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.*

//import com.novaplus.retailer.view.bottom_nav.BottomHostActivity

@SuppressLint("CustomSplashScreen")
@DelicateCoroutinesApi
@InternalCoroutinesApi
class SplashScreenActivity : AppCompatActivity() {

    lateinit var splashviewModel: SplashScreenViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        splashviewModel = ViewModelProvider(this).get(SplashScreenViewModel::class.java)

        checkLoggedIn()
        CheckAuth()
        observe()


    }


    private fun CheckAuth() {
        try {


            if (checkLoggedIn() && App().sharedPrefs.login) {
                Log.d("checkLoggedIn", "CheckAuth: " + checkLoggedIn())
                GlobalScope.launch(Dispatchers.Main) {
                    try {


                        splashviewModel.getItemsdetails(applicationContext)
                            ?.observe(this@SplashScreenActivity, Observer { itemlist ->

                                Log.d("Item check", "CheckAuth: " + itemlist)

                                //check product table is notempty  else fetch data
                                if (!itemlist.isNullOrEmpty()) {
                                    GlobalScope.launch {
                                        delay(2000)
                                        val intent =
                                            Intent(
                                                applicationContext,
                                                BottomHostActivity::class.java
                                            )
                                        intent.putExtra(
                                            "fragmentName",
                                            BottomHostActivity.Pages.DASHBOARD
                                        )
                                        intent.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                    }
                                } else {
                                    getData()
                                }

                            })


                    } catch (e: Exception) {
                        Log.d("Exception", "CheckAuth: " + e.localizedMessage)
                        Toasty.error(this@SplashScreenActivity, "Something went wrong").show()
                    }
                }


            } else {
                GlobalScope.launch {
                    delay(2000)
                    val intent = Intent(applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
            }
        } catch (e: java.lang.Exception) {
        }
    }

    private fun observe() {
        splashviewModel.getDataStatus.observe(this, Observer { itemdatas ->
            Loader.hideLoader()
            if (itemdatas.status!!.equals("success")) {
                Log.d("Categories ", "GetData: " + itemdatas.categories)
                Log.d("Products", "GetData: " + itemdatas.products)
                val intent = Intent(applicationContext, BottomHostActivity::class.java)
                intent.putExtra("fragmentName", BottomHostActivity.Pages.DASHBOARD)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(this, itemdatas.message, Toast.LENGTH_LONG).show()
                val intent = Intent(applicationContext, BottomHostActivity::class.java)
                intent.putExtra("fragmentName", BottomHostActivity.Pages.DASHBOARD)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        })
    }

    fun getData() {

        if (this.isConnectedToNetwork()) {
            Loader.showLoader(this)
            GlobalScope.launch {
                splashviewModel.getAllData(applicationContext)
            }

        } else {
            Toast.makeText(this, "Please connect to internet", Toast.LENGTH_LONG).show()
        }
    }

    fun checkLoggedIn(): Boolean {
        return App().sharedPrefs.user_id != null
    }


    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

}
