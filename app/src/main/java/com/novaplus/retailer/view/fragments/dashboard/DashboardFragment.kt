package com.novaplus.retailer.view.fragments.dashboard

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.commons.Loader
import com.azinova.model.cart.CartCommon
import com.azinova.model.cart.ItemDetails
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.utils.ActivityUtils
import com.novaplus.retailer.view.adapter.*
import com.novaplus.retailer.view.fragments.payment.PaymentFragment
import es.dmoral.toasty.Toasty
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_bottom_host.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.create_customer.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.coroutines.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

@DelicateCoroutinesApi
@InternalCoroutinesApi
class DashboardFragment : Fragment(), ProductListAdapter.ProductListInterface,
    CartAdapter.CartListInterface {

    private val disposable = CompositeDisposable()

    lateinit var dashboardViewModel: DashboardViewModel

    lateinit var categoryAdapter: CategoryAdapter
    lateinit var productListAdapter: ProductListAdapter

    var allproductlist: List<ItemTable>? = ArrayList()

    var cartlist: List<CartItemTable>? = ArrayList()
    var selectedcartitem: CartItemTable? = null

    lateinit var cartAdapter: CartAdapter
    lateinit var selectcustomeradapter: SelectCustomerAdapter
    private lateinit var rv_customerlist: RecyclerView

    var SearchList = ArrayList<ItemTable>()
    var seletedcustomer_details: ArrayList<CustomersItem>? = null

    var selected_customer: String = ""
    var selected_customerId: Int = 0
    var selected_cartId: Int = 0

    private var IsQtyClicked = false
    private var IsDiscClicked = false
    private var IsPriceClicked = false

    var charged_amount :Double? = 0.00
    var subtotal :Double? = 0.00
    var tax :Double? = 0.00
    var discount :Int? = 0

    val commoncart = CartCommon()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.ll_menu?.visibility = View.VISIBLE
        activity?.ll_search?.visibility = View.VISIBLE

        Log.d("Test 1", "onViewCreated: ")
        dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

        onViewset()

        init()

        onClick()
        onObserve()

        recyclerViewCart.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        )


        rv_categorys.setLayoutManager(
            LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        )
        rv_products.setLayoutManager(GridLayoutManager(requireContext(), 4))

        activity?.searchtext?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                SearchList.clear()
                Log.d("TAG", "afterTextChanged: " + allproductlist)
                if (editable.toString().equals("")) {
                    productListAdapter.refreshData(allproductlist!!)
                } else {

                    if (allproductlist?.size!! > 0) {
                        for (item in allproductlist!!) {
                            if (item.barcode!!.contains(editable.toString(), ignoreCase = true)) {
                                SearchList.add(item!!)
                            }
                        }
                        productListAdapter.refreshData(SearchList!!)
                    }


                }
            }
        })

    }


    private fun onObserve() {
        dashboardViewModel.getAllCustomers.observe(viewLifecycleOwner, Observer { CustomerList ->
            Loader.hideLoader()
            if (CustomerList.status.equals("success")) {
                Log.d("Customers ", "GetCustomers: " + CustomerList.customers)

                if (CustomerList.customers?.size != 0) {
                    val customerbuilder = AlertDialog.Builder(requireContext())
                    val customerview: View =
                        LayoutInflater.from(requireContext())
                            .inflate(R.layout.dialog_customerlist, null)
                    customerbuilder.setView(customerview)
                    val customeralertDialog: AlertDialog = customerbuilder.create()

                    rv_customerlist = customerview.findViewById(R.id.rv_customerlist)

                    rv_customerlist.setLayoutManager(
                        LinearLayoutManager(
                            context,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                    )
                    selectcustomeradapter =
                        SelectCustomerAdapter(requireContext(), CustomerList.customers) {
                            seletedcustomer_details?.clear()
                            if (it != null) {
                                seletedcustomer_details?.addAll(listOf(it))
                            }
                            selected_customer = it?.name.toString()
                            selected_customerId = it?.id!!
                            Thread {
                                dashboardViewModel.updateCustomer(
                                    requireContext(),
                                    it.id!!, it.name.toString()
                                )
                            }.start()


                            Toasty.success(
                                requireContext(),
                                it?.name + " Selected",
                                Toast.LENGTH_SHORT
                            ).show()
                            cl_subtotal.visibility = View.VISIBLE
                            cl_expand_view.visibility = View.GONE

                            customeralertDialog.dismiss()
                        }
                    rv_customerlist.adapter = selectcustomeradapter

                    customeralertDialog.show()
                } else {

                    Toast.makeText(requireContext(), "Customer List is empty", Toast.LENGTH_LONG)
                        .show()
                }


            } else {
                Toast.makeText(requireContext(), CustomerList.message, Toast.LENGTH_LONG).show()

            }
        })


    }

    private fun init() {

    }

    private fun onClick() {
        img_edit.setOnClickListener(View.OnClickListener {
            Toasty.warning(requireContext(), "Coming Soon", Toast.LENGTH_SHORT).show()

        })

        img_delete_item.setOnClickListener(View.OnClickListener {
            if (cartlist.isNullOrEmpty()) {
                Toasty.warning(requireContext(), "Cart is empty", Toast.LENGTH_SHORT).show()
            } else {

                if (commoncart.selected_pos == -1) {
                    Toasty.warning(requireContext(), "Please Select Item", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    Log.d("selected_cartId ", "onClick: " + commoncart.selected_cartid)


                    val alertDialog = AlertDialog.Builder(requireContext())
                        .setTitle("Remove Item From Cart")
                        .setMessage("Are you sure, Want to remove this item !")

                        .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->


                            GlobalScope.launch {
                                dashboardViewModel.removerFromCart(
                                    requireContext(),
                                    commoncart.selected_cartid,
                                    commoncart.selected_item_details?.cartQty,
                                    commoncart.selected_item_details?.price,
                                    commoncart.selected_item_details?.total
                                )
                            }

                            Toasty.success(
                                requireContext(),
                                "Item Deleted",
                                Toast.LENGTH_SHORT
                            ).show()



                            dialog.dismiss()
                        })
                        .setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                            dialog.dismiss()
                        })
                    val alert = alertDialog.create()
                    alert.show()


                }
            }

        })

        img_up_arrow.setOnClickListener(View.OnClickListener {
            cl_subtotal.visibility = View.GONE
            cl_expand_view.visibility = View.VISIBLE
        })

        img_ll_down.setOnClickListener {
            cl_subtotal.visibility = View.VISIBLE
            cl_expand_view.visibility = View.GONE
        }

        btn_customer.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {
                if (requireContext().isConnectedToNetwork()) {
                    Loader.showLoader(requireContext())
                    dashboardViewModel.getAllCustomers()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        btn_quatation.setOnClickListener {
            Toasty.warning(requireContext(), "In Progress", Toast.LENGTH_SHORT).show()
        }
        btn_reward.setOnClickListener {
            Toasty.warning(requireContext(), "In Progress", Toast.LENGTH_SHORT).show()
        }
        btn_refund.setOnClickListener {
            Toasty.warning(requireContext(), "In Progress", Toast.LENGTH_SHORT).show()
        }
        btn_info.setOnClickListener {
            Toasty.warning(requireContext(), "In Progress", Toast.LENGTH_SHORT).show()
        }

        paymentButton.setOnClickListener {

            if (cartlist.isNullOrEmpty()) {
                Toasty.warning(requireContext(), "Cart is empty", Toast.LENGTH_SHORT).show()
            } else if (txt_scustomer.text.toString().equals("") || txt_scustomer.text.toString()
                    .equals("N/a") ||
                txt_scustomer.text.toString().equals(null)
            ) {
                Toasty.warning(requireContext(), "Please select customer", Toast.LENGTH_SHORT)
                    .show()
            } else {
                GlobalScope.launch {
                    dashboardViewModel.totalChargeUpdate(
                        requireContext(),
                        charged_amount!!
                    )
                }

                val paymentFragment: Fragment = PaymentFragment()

                val model_bundle = Bundle()
                model_bundle.putString("from", "Dashboard")
                model_bundle.putString("Total Amount", charged_amount.toString())
                model_bundle.putString("Subtotal", subtotal.toString())
                model_bundle.putString("Tax", tax.toString())
                model_bundle.putString("Discount", discount.toString())

                paymentFragment.arguments = model_bundle
                ActivityUtils.replaceFragment(
                    parentFragmentManager,
                    paymentFragment,
                    R.id.main_content
                )

            }


        }// btn_payment


        txt_qty.setOnClickListener {

            Log.d("Check", "Common Cart : " + commoncart)

            if (IsQtyClicked) {
                IsQtyClicked = false
                txt_qty.setBackgroundResource(R.drawable.bg_rect_nonselect)
            } else {
                IsQtyClicked = true
                txt_qty.setBackgroundResource(R.drawable.bg_rect_select)

                IsDiscClicked = false
                txt_disc.setBackgroundResource(R.drawable.bg_rect_nonselect)
                IsPriceClicked = false
                txt_price.setBackgroundResource(R.drawable.bg_rect_nonselect)


                if (cartlist?.size!! > 0) {

                }
                /*  else{
                      Toasty.warning(requireContext(), "Please Select Item", Toast.LENGTH_SHORT)
                          .show()
                  }*/

            }
        }
        txt_disc.setOnClickListener {
            if (IsDiscClicked) {
                IsDiscClicked = false
                txt_disc.setBackgroundResource(R.drawable.bg_rect_nonselect)
            } else {
                IsDiscClicked = true
                txt_disc.setBackgroundResource(R.drawable.bg_rect_select)

                IsQtyClicked = false
                txt_qty.setBackgroundResource(R.drawable.bg_rect_nonselect)
                IsPriceClicked = false
                txt_price.setBackgroundResource(R.drawable.bg_rect_nonselect)


            }
        }
        txt_price.setOnClickListener {
            if (IsPriceClicked) {
                IsPriceClicked = false
                txt_price.setBackgroundResource(R.drawable.bg_rect_nonselect)
            } else {
                IsPriceClicked = true
                txt_price.setBackgroundResource(R.drawable.bg_rect_select)

                IsQtyClicked = false
                txt_qty.setBackgroundResource(R.drawable.bg_rect_nonselect)
                IsDiscClicked = false
                txt_disc.setBackgroundResource(R.drawable.bg_rect_nonselect)
            }
        }

        txt_del.setOnClickListener {
            Log.d("Cartselect", "numberEngine: " + commoncart)

            if (cartlist?.size!! > 0) {
                if (IsQtyClicked) {
                    if (!(commoncart.selected_pos == -1)) {

                        val qtyy: Int? = commoncart.selected_item_details!!.cartQty
                        var newqty = removeLastChar(qtyy.toString())
                        Log.d("TAG", "numberEngine: " + newqty)
                        if (newqty == "") {
                            newqty = "0"
                        }
                        GlobalScope.launch {
                            dashboardViewModel.quantityUpdate(
                                requireContext(),
                                commoncart.selected_cartid,
                                newqty
                            )
                        }
                        Toasty.success(
                            requireContext(),
                            "Quantity Updated",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                } else if (IsDiscClicked) {
                    val disc: Int? =
                        cartlist!!.get(0).discount//commoncart.selected_item_details!!.discount
                    Log.d("Disc Del", "numberEngine: " + disc)

                    var newdisc = ""
                    if (disc != null) {
                        if (disc.equals(0)) {
                        } else {
                            newdisc = removeLastChar(disc.toString())
                            Log.d("Disc Del", "numberEngine: " + newdisc)
                        }
                    }

                    if (newdisc == "") {
                        newdisc = "0"
                    }

                    GlobalScope.launch {
                        dashboardViewModel.discountUpdate(
                            requireContext(),
                            commoncart.selected_cartid,
                            newdisc
                        )
                    }

                    cl_subtotal.visibility = View.VISIBLE
                    cl_expand_view.visibility = View.GONE
                    Toasty.success(
                        requireContext(),
                        "Discount Updated",
                        Toast.LENGTH_SHORT
                    ).show()


                } else if (IsPriceClicked) {
                }
            }
        }


        num_1.setOnClickListener {
            numberEngine(1)
        }
        num_2.setOnClickListener {
            numberEngine(2)
        }
        num_3.setOnClickListener {
            numberEngine(3)
        }
        num_4.setOnClickListener {
            numberEngine(4)
        }
        num_5.setOnClickListener {
            numberEngine(5)
        }
        num_6.setOnClickListener {
            numberEngine(6)
        }
        num_7.setOnClickListener {
            numberEngine(7)
        }
        num_8.setOnClickListener {
            numberEngine(8)
        }
        num_9.setOnClickListener {
            numberEngine(9)
        }
        num_zero.setOnClickListener {
            numberEngine(0)
        }

    }

    private fun removeLastChar(str: String): String {
        return str.substring(0, str.length - 1)
    }

    private fun numberEngine(i: Int) {
        Log.d("Cartselect", "numberEngine: " + commoncart)
        try {
            if (cartlist?.size!! > 0) {
                if (IsQtyClicked) {
                    if (commoncart.selected_pos == -1) {
                        Toasty.warning(
                            requireContext(),
                            "Please select an item",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {

                        if (commoncart.selected_item_details!!.cartQty!! > 99) return

                        val qtyy: Int? = commoncart.selected_item_details!!.cartQty
                        Log.d("TAG", "numberEngine: " + qtyy)
                        var newqtyy = ""
                        if (qtyy!!.equals("0")) {
                            newqtyy = i.toString()
                        } else {
                            newqtyy = "$qtyy$i"
                        }
                        Log.d("TAG 1", "numberEngine: " + newqtyy)
                        if (newqtyy.length > 2) return
                        if (newqtyy > commoncart.selected_item_details!!.available_qty.toString()) {
                            Toasty.warning(requireContext(), "Quantity exceed", Toasty.LENGTH_SHORT)
                                .show()
                            return
                        }

                        GlobalScope.launch {
                            dashboardViewModel.quantityUpdate(
                                requireContext(),
                                commoncart.selected_cartid,
                                newqtyy
                            )
                        }
                        Toasty.success(
                            requireContext(),
                            "Quantity Updated",
                            Toast.LENGTH_SHORT
                        ).show()

                    }
                } else if (IsDiscClicked) {
                    var discount =
                        cartlist!!.get(0).discount//commoncart.selected_item_details?.discount
                    if (discount != null) {
                    } else {
                        discount = 0
                    }

                    Log.d("discount", "numberEngine: " + discount)
                    var newdiscount = ""
                    if (discount.equals(0)) {
                        newdiscount = i.toString()
                    } else {
                        newdiscount = "$discount$i"
                    }

                    if (newdiscount.toInt() > 99) return


                    GlobalScope.launch {
                        dashboardViewModel.discountUpdate(
                            requireContext(),
                            commoncart.selected_cartid,
                            newdiscount
                        )
                    }
                    cl_subtotal.visibility = View.VISIBLE
                    cl_expand_view.visibility = View.GONE
                    Toasty.success(
                        requireContext(),
                        "Discount Updated",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (IsPriceClicked) {
                }
            }
        } catch (e: Exception) {
        }
    }




    override fun onResume() {
        super.onResume()
        Log.d("Test 2", "onViewCreated: ")
        activity?.ll_menu?.visibility = View.VISIBLE
        activity?.ll_search?.visibility = View.VISIBLE
        onViewset()
    }

    @DelicateCoroutinesApi
    private fun onViewset() {

        GlobalScope.launch(Dispatchers.Main) {

            dashboardViewModel.getAllCartitems(requireContext())?.observe(viewLifecycleOwner,
                Observer { newcartlist ->
                    try {

                        commoncart.selected_pos = -1

                        cartlist = newcartlist
                        var sum: Double? = 0.00
                        var taxx: Double? = 0.00
                        if (newcartlist?.size!! > 0) {
                            Log.d("cartlist size", "onViewset: " + newcartlist.size.toString())
                            recyclerViewCart.visibility = View.VISIBLE
                            txt_cart_count.visibility = View.VISIBLE
                            txt_cart_count.text = newcartlist.size.toString()

                            try {


                                for (i in newcartlist) {

                                    sum = sum?.plus(i.price!! * i.cartQty!!)?.toDouble()
                                    taxx = taxx?.plus(i.tax?.toDouble()!!)?.toDouble()


                                    Log.d("Sum", "onViewset: " + sum)
                                    Log.d("Taxx", "onViewset: " + taxx)

                                }

                                val sum3digits: Double = Math.round(sum!! * 1000.0) / 1000.0
                                Log.d("sum3digits", "onViewset: " + sum3digits)
                                val sum2digits: Double = Math.round(sum3digits * 100.0) / 100.0
                                Log.d("sum2digits", "onViewset: " + sum2digits)

                                val taxx3digits: Double = Math.round(taxx!! * 1000.0) / 1000.0
                                Log.d("taxx3digits", "onViewset: " + taxx3digits)
                                val taxx2digits: Double = Math.round(taxx3digits * 100.0) / 100.0
                                Log.d("taxx2digits", "onViewset: " + taxx2digits)


                                subtotal = sum2digits
                                tax = taxx2digits

                                txt_subtotal.text = "Subtotal : AED " + sum2digits
                                txt_tax.text = "Tax : AED " + taxx2digits

                                val total = sum?.plus(taxx!!)
                                val total3digits: Double = Math.round(total!! * 1000.0) / 1000.0
                                Log.d("total3digits", "onViewset: " + total3digits)
                                val total2digits: Double = Math.round(total3digits * 100.0) / 100.0
                                Log.d("total2digits", "onViewset: " + total2digits)

                                txt_total.text = "Total: AED " + total2digits

                                val percnt = newcartlist.get(0).discount!!
                                val disprice = total - ((total.toInt() * percnt) / 100)
                                Log.d("percnt", "onViewset: " + percnt)
                                Log.d("disprice", "onViewset: " + disprice)

                                discount = percnt
                                charged_amount = disprice

                                paymentButton.text = "CHARGE    AED " + disprice

                                txt_Discount.text =
                                    "Discount : " + percnt + "%"

                                txt_scustomer.text = newcartlist.get(0).customer_name!!

                                commoncart.total = total.toString()
                                App().sharedPrefs.cart_details?.total = total.toString()

                            } catch (e: Exception) {
                                Log.d("Exception", "onViewset: " + e.localizedMessage)
                            }

                            cartAdapter =
                                CartAdapter(
                                    requireContext(),
                                    newcartlist!!,
                                    this@DashboardFragment,
                                    commoncart.selected_cartid
                                )
                            recyclerViewCart.adapter = cartAdapter


                        } else {
                            recyclerViewCart.visibility = View.GONE
                            txt_cart_count.visibility = View.GONE

                            charged_amount = 0.00
                            subtotal = 0.00
                            tax = 0.00
                            discount = 0

                            txt_subtotal.text = "Subtotal : AED 00.00"
                            txt_tax.text = "Tax : AED 00.00"
                            txt_total.text = "Total: AED 00.00"
                            paymentButton.text = "CHARGE    AED 00.00"
                            txt_Discount.text = "Discount : 0%"
                            txt_scustomer.text = "N/a"
                        }

                    } catch (e: Exception) {
                    }


                })

        }


        //allcategorysFromdb
        GlobalScope.launch(Dispatchers.Main) {
            dashboardViewModel.getallcategorys(requireContext())
                ?.observe(viewLifecycleOwner, Observer { categorylist ->
                    Log.d("getallcategorys", "onViewset: " + categorylist)

                    try {


                        if (!categorylist.isNullOrEmpty()) {
                            categoryAdapter = CategoryAdapter(requireContext(), categorylist) {
                                Log.d("Adapter", "onViewset: " + it)

                                GlobalScope.launch(Dispatchers.Main) {


                                    if (it?.category_id == 1) {



                                        dashboardViewModel.getAllproducts(requireContext())
                                            ?.observe(
                                                viewLifecycleOwner,
                                                Observer { allproductlistFromDb ->
                                                    Log.d(
                                                        "TAG",
                                                        "product check: " + allproductlistFromDb
                                                    )

                                                    allproductlist = allproductlistFromDb

                                                    if (!allproductlistFromDb.isNullOrEmpty()) {
                                                        rv_products.visibility = View.VISIBLE
                                                        Log.d(
                                                            "Adapter Allproducts",
                                                            "onViewset: " + allproductlistFromDb
                                                        )
                                                        productListAdapter = ProductListAdapter(
                                                            requireContext(),
                                                            allproductlistFromDb,
                                                            this@DashboardFragment
                                                        )
                                                        rv_products.adapter = productListAdapter

                                                        fasttrcv.handleNormalColor =
                                                            ContextCompat.getColor(
                                                                requireContext(),
                                                                R.color.blue_light
                                                            )
                                                        fasttrcv.handlePressedColor =
                                                            ContextCompat.getColor(
                                                                requireContext(),
                                                                R.color.blue_light
                                                            )
                                                        fasttrcv.attachRecyclerView(rv_products)

                                                    } else {
                                                        rv_products.visibility = View.GONE

                                                    }

                                                })


                                    } else {

                                        dashboardViewModel.getproductsbycategory(
                                            requireContext(),
                                            it?.category_id
                                        )
                                            ?.observe(
                                                viewLifecycleOwner,
                                                Observer { allproductlistFromDb ->
                                                    Log.d(
                                                        "Adapter products",
                                                        "onViewset: " + allproductlistFromDb
                                                    )
                                                    allproductlist = allproductlistFromDb

                                                    if (!allproductlistFromDb.isNullOrEmpty()) {
                                                        rv_products.visibility = View.VISIBLE
                                                        Log.d(
                                                            "Adapter products",
                                                            "onViewset: " + allproductlistFromDb
                                                        )
                                                        productListAdapter = ProductListAdapter(
                                                            requireContext(),
                                                            allproductlistFromDb,
                                                            this@DashboardFragment
                                                        )
                                                        rv_products.adapter = productListAdapter

                                                        fasttrcv.handleNormalColor =
                                                            ContextCompat.getColor(
                                                                requireContext(),
                                                                R.color.blue_light
                                                            )
                                                        fasttrcv.handlePressedColor =
                                                            ContextCompat.getColor(
                                                                requireContext(),
                                                                R.color.blue_light
                                                            )
                                                        fasttrcv.attachRecyclerView(rv_products)
                                                    } else {
                                                        rv_products.visibility = View.GONE
                                                    }


                                                })


                                    }
                                }


                            }
                            rv_categorys?.setAdapter(categoryAdapter)
                        }

                    } catch (e: Exception) {
                    }
                })
        }


    }

    @SuppressLint("MissingPermission")
    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

    override suspend fun clickProducts(getItem: ItemTable) {
        Log.d("addToCart", "clickProducts: " + getItem)
        charged_amount = charged_amount?.plus(getItem.price!!)
        Log.d("addToCart total", "clickProducts: " + charged_amount)

        Log.d("seletedcustomer_details", "clickProducts: " + seletedcustomer_details)

        if (getItem.qty_available!! > 0) {
            dashboardViewModel.addToCartApiCall(
                getItem,
                requireContext(),
                selected_customer,
                selected_customerId,
                charged_amount.toString()
            )
        } else {
            Toasty.warning(requireContext(), "Item out of stock").show()
        }
    }

    override fun showNotification(message: String?) {
        // showNotificationMessage(message!!)
    }


    private fun showNotificationMessage(msge: String) {
        object : CountDownTimer(1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                relative_add_cart.setVisibility(View.VISIBLE)
                textView_notification_message.setText(msge)
                imageView_close_notification.setOnClickListener(
                    View.OnClickListener { v: View? ->
                        relative_add_cart.setVisibility(
                            View.GONE
                        )
                    }
                )
            }

            override fun onFinish() {
                relative_add_cart.setVisibility(View.GONE)
            }
        }.start()
    }

    override fun clickItems(getItem: CartItemTable, position: Int) {

        selectedcartitem = getItem

        Log.d("getItem", "clickItems: " + selectedcartitem)


        commoncart.selected_pos = position
        commoncart.selected_cartid = getItem.cart_id
        commoncart.selected_item_details =
            ItemDetails(
                cart_id = getItem.cart_id,
                item_id = getItem.item_id,
                item_name = getItem.item_name,
                price = getItem.price,
                cost = getItem.cost,
                tax = getItem.tax,
                cartQty = getItem.cartQty,
                discount = getItem.discount,
                customer_id = getItem.customer_id,
                customer_name = getItem.customer_name,
                available_qty = getItem.qty_available,
                total = getItem.total
            )



        App().sharedPrefs.cart_details?.selected_pos = position


    }

    override fun itemUnselect(getItem: CartItemTable, position: Int) {
        commoncart.selected_pos = -1
        commoncart.selected_cartid = 0
        commoncart.selected_item_details = null

    }

    override fun qtyPlus(cartItemTable: CartItemTable, pls: Int?) {
        GlobalScope.launch {
            dashboardViewModel.quantityUpdate(
                requireContext(),
                cartItemTable.cart_id,
                pls.toString()
            )
        }
    }

    override fun qtyMinus(cartItemTable: CartItemTable, min: Int?) {
        GlobalScope.launch {
            dashboardViewModel.quantityUpdate(
                requireContext(),
                cartItemTable.cart_id,
                min.toString()
            )
        }
    }


}