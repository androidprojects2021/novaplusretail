package com.azinova.barcodeprinter.utils

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object ThreadPoolmanager {

    private var service: ExecutorService? = null

    init {
        val num = Runtime.getRuntime().availableProcessors() * 20
        service = Executors.newFixedThreadPool(num)
    }

    fun executeTask(runnable: Runnable?) {
        service!!.execute(runnable)
    }

}