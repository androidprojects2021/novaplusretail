package com.novaplus.retailer.view.bottomsheet
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.novaplus.retailer.R
import com.novaplus.retailer.view.fragments.dashboard.DashboardViewModel
import kotlinx.coroutines.InternalCoroutinesApi

class CartBottomsheet: BottomSheetDialogFragment() {

    @InternalCoroutinesApi
    lateinit var viewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.cart_bottom_sheet, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       // viewModel = ViewModelProvider(this)[DashboardViewModel::class.java]

        onClick(view)
    }

    private fun onClick(view: View) {



    }


    fun checkInternet(): Boolean {

        val connectivityManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetWorkInfo = connectivityManager.activeNetworkInfo
        return activeNetWorkInfo != null && activeNetWorkInfo.isConnected

    }
}

