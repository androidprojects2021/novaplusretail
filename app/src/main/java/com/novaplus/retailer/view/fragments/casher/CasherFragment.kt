package com.novaplus.retailer.view.fragments.casher

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.novaplus.retailer.R
import com.novaplus.retailer.view.fragments.dashboard.DashboardViewModel


class CasherFragment : Fragment() {

    lateinit var cashierViewModel:  CashierViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_casher, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cashierViewModel = ViewModelProvider(this).get(CashierViewModel::class.java)

    }


}