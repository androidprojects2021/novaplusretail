package com.novaplus.retailer.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.cart.CartCommon
import com.novaplus.retailer.App
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.CartItemTable
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.adapter_cart_recycler.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.DecimalFormat

class CartAdapter(
    val context: Context,
    var cartItemList: List<CartItemTable>,
    val cartListInterface: CartListInterface,
    val selectedCartid: Int?
) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    var format = DecimalFormat("##.00")
    private val prefs = App().sharedPrefs

    private var selectedPosition = -1
    val commoncart = CartCommon()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_cart_recycler, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cartItemList.size
    }

    override fun onBindViewHolder(holder: CartAdapter.ViewHolder, position: Int) {

        Log.d("TAG", "onBindViewHolder: " + selectedCartid)
/*
        if (selectedCartid == cartItemList[position].cart_id) {
            selectedPosition = position
        }*/

        if (cartItemList[position].cartQty!!.equals(1)) {
            holder.itemView.quantity_minus.visibility = View.INVISIBLE
        } else {
            holder.itemView.quantity_minus.visibility = View.VISIBLE
        }



        holder.itemView.cart_item_name.text = cartItemList[position].item_name
        val totalprice = (cartItemList[position].price!! * cartItemList[position].cartQty!!)

        val total3digits: Double = Math.round(totalprice!! * 1000.0) / 1000.0
        Log.d("total3digits", "onViewset: " + total3digits)
        val total2digits: Double = Math.round(total3digits * 100.0) / 100.0
        Log.d("total2digits", "onViewset: " + total2digits)

        holder.itemView.txt_item_price.text = "AED " + total2digits
        holder.itemView.txt_cart_quantity.text = cartItemList[position].cartQty.toString()

        if (position == selectedPosition) {
            holder.itemView.ll_cartitem.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.light_green
                )
            )
            GlobalScope.launch(Dispatchers.Main) {
                cartListInterface.clickItems(cartItemList[position], position)
            }
        } else {
            holder.itemView.ll_cartitem.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
        }


        holder.itemView.setOnClickListener(View.OnClickListener {
            if (position == selectedPosition) {
                selectedPosition = -1

                cartListInterface.itemUnselect(cartItemList[position], position)

                holder.itemView.ll_cartitem.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.white
                    )
                )
            } else {

                onStateChangedListener(position, holder)
            }

            // cartListInterface.clickItems(cartItemList[position])

        })

        holder.itemView.quantity_minus.setOnClickListener {
            val min = (cartItemList[position].cartQty?.minus(1))

            cartListInterface.qtyMinus(cartItemList[position], min)
            Log.d("Cart Qty", "quantity_minus: " + min)
        }
        holder.itemView.quantity_plus.setOnClickListener {
            val pls = (cartItemList[position].cartQty?.plus(1))

            if (pls != null) {
                if ((pls.toString().length > 2) || (pls > cartItemList[position].qty_available!!)) {
                    Log.d("Cart Qty", "quantity_minus: " + pls)
                    Log.d("Cart Qty", "quantity_length: " + pls.toString().length)
                    Log.d("Cart Qty", "quantity_available: " + cartItemList[position].qty_available.toString())

                    Toasty.warning(context, "Quantity exceed", Toasty.LENGTH_SHORT)
                        .show()

                } else {
                    cartListInterface.qtyPlus(cartItemList[position], pls)
                }
            }

            Log.d("Cart Qty", "quantity_minus: " + pls)
        }

    }

    private fun onStateChangedListener(position: Int, holder: CartAdapter.ViewHolder) {
        selectedPosition = position
        holder.itemView.ll_cartitem.setBackgroundColor(
            ContextCompat.getColor(
                context,
                R.color.light_green
            )
        )
        GlobalScope.launch(Dispatchers.Main) {
            cartListInterface.clickItems(cartItemList[position], position)
        }

        notifyDataSetChanged()
    }


    fun refreshData(cartItemList: List<CartItemTable>) {
        this.cartItemList = cartItemList
        notifyDataSetChanged()
    }

    interface CartListInterface {
        fun clickItems(get: CartItemTable, position: Int)
        fun itemUnselect(cartItemTable: CartItemTable, position: Int)
        fun qtyPlus(cartItemTable: CartItemTable, pls: Int?)
        fun qtyMinus(cartItemTable: CartItemTable, min: Int?)
    }

    class SelectedItem(val position: Int, val data: CartItemTable)
}