package com.novaplus.retailer.views.auth.login


import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.InputLogin
import com.azinova.model.response.data.ResponseGetData
import com.azinova.model.response.login.LoginResponse
import com.azinova.model.response.login.Result
import com.novaplus.retailer.App
import com.novaplus.retailer.database.repository.ProductRepository
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@DelicateCoroutinesApi
@InternalCoroutinesApi
class LoginViewModel : ViewModel() {

    private var _loginResponse = MutableLiveData<LoginResponse>()
    val loginResponse: LiveData<LoginResponse>
        get() = _loginResponse

    private val _getDataStatus = MutableLiveData<ResponseGetData>()
    val getDataStatus: LiveData<ResponseGetData>
        get() = _getDataStatus


    fun login(inputLogin: InputLogin) {
        viewModelScope.launch {
            try {


                val response =  App().retrofit.loginApiCall(inputLogin)

                Log.d("ViewModel", "getlogin: " + response)

                if (response.result?.status.equals("success")) {
                    _loginResponse.value = response
                } else {
                    _loginResponse.value = response
                }

            } catch (e: Exception) {
                _loginResponse.value = LoginResponse(Result(message = "Something Went Wrong", status = "Error"))
                Log.d("test",e.toString())
            }
        }

    }



  suspend fun getAllData(context: Context) {


        Log.d("Api Call - ViewModel", "getData: ")

            try {
                val response = App().retrofit.getData()

                Log.d("ViewModel", "getData: " + response)

                if (response.status.equals("success")) {
                    insertToDb(context, response)
                    _getDataStatus.value = response
                } else {
                    _getDataStatus.value = response

                }

            } catch (e: java.lang.Exception) {
                _getDataStatus.value =
                    ResponseGetData(message = "Something went wrong", status = "error")
            }



    }
    private suspend fun insertToDb(context: Context, response: ResponseGetData) {
        ProductRepository.insertData(context, response.categories, response.products)
    }

}