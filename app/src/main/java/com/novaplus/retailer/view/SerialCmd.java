package com.novaplus.retailer.view;

public class SerialCmd {
    public static final String PREFIX_HEX = "7E0130303030";
    public static final String STORAGE_EVER_HEX = "40";
    public static final String STORAGE_TEMP_HEX = "23";
    public static final String SUFFIX_HEX = "3B03";

    public static final String SUBTAG_QUERY_CURRENT_HEX = "2A";
    public static final String SUBTAG_QUERY_FACTORY_HEX = "26";
    public static final String SUBTAG_QUERY_RANGE_HEX = "5E";

    public static final String RES_PREFIX_HEX = "020130303030";
    public static final String RES_ACK_HEX = "06";
    public static final String RES_NAK_HEX = "15";
    public static final String RES_ENQ_HEX = "05";

    public static final int MIN_SEND_TIME = 50;        
    public static final int MAX_RESPONSE_TIME = 5000;

    public static final String NLS_SETUPE1 = "#SETUPE1";
    public static final String NLS_SETUPE0 = "#SETUPE0";

    public static final String NLS_KEY_DOWN = "#SCNTRG1";
    public static final String NLS_KEY_UP = "#SCNTRG0";

    public static final String NLS_RESTORE = "@FACDEF";

    public static String setExposure(int level){
        return "@EXPLVL" + level;
    }


    public static String setScanMode(int mode, int wait, int delay){
        switch (mode) {
            case 0:
                return ("@SCNMOD0;ORTSET"+(wait>0 ? wait : 60000));
            case 1:
                return ("@SCNMOD2;ORTSET"+(wait>0 ? wait : 1000)+";" + "RRDDUR"+(delay>=200 ? delay : 1000));
            default:
                return null;
        }
    }


    public static String queryScanMode(){
        return "#SCNMOD*;ORTSET*;RRDDUR*";
    }
}
