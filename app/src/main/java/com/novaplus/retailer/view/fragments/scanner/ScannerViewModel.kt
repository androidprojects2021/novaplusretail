package com.novaplus.retailer.view.fragments.scanner

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.novaplus.retailer.database.repository.ProductRepository
import com.novaplus.retailer.database.tables.ItemTable
import kotlinx.coroutines.InternalCoroutinesApi
@InternalCoroutinesApi
class ScannerViewModel:ViewModel() {

    var allproductlistFromDb: LiveData<List<ItemTable>?>? = null



    fun getAllproducts(
        requireContext: Context
    ): LiveData<List<ItemTable>?>? {
        allproductlistFromDb =
            ProductRepository.getItemsdetails(requireContext)
        return allproductlistFromDb
    }

}