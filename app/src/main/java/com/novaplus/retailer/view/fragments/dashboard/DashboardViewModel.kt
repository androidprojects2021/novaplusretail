package com.novaplus.retailer.view.fragments.dashboard

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.model.response.customers_list.CustomerListResponse

import com.novaplus.retailer.App
import com.novaplus.retailer.database.repository.CartRespository
import com.novaplus.retailer.database.repository.ProductRepository
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.database.tables.CategoryTable
import com.novaplus.retailer.database.tables.ItemTable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import java.lang.Exception

@InternalCoroutinesApi
class DashboardViewModel : ViewModel() {
    private val prefs = App().sharedPrefs

    // var cartItemListFromDb: List<CartItemTable>? = null
    var cartItemListFromDb: LiveData<List<CartItemTable>?>? = null

    // var categorylistFromDb: List<CategoryTable>? = null
    var categorylistFromDb: LiveData<List<CategoryTable>?>? = null

    //var productlistFromDbbycategory: List<ItemTable>? = null
    var productlistFromDbbycategory: LiveData<List<ItemTable>?>? = null

    // var allproductlistFromDb: List<ItemTable>? = null
    var allproductlistFromDb: LiveData<List<ItemTable>?>? = null

    val updateCartResponse = MutableLiveData<Boolean>()

    private val _getAllCustomers = MutableLiveData<CustomerListResponse>()
    val getAllCustomers: LiveData<CustomerListResponse>
        get() = _getAllCustomers


    init {
        updateCartResponse.value = false
    }


    /* suspend fun getAllCategories(requireContext: Context): List<CategoryTable>? {
         categorylistFromDb = ProductRepository.getAllCategories(requireContext)
         return categorylistFromDb
     }*/


    fun getallcategorys(requireContext: Context): LiveData<List<CategoryTable>?>? {
        categorylistFromDb = ProductRepository.getAllCategories(requireContext)
        return categorylistFromDb
    }


    /* suspend fun getAllproducts(
         requireContext: Context
     ): List<ItemTable>? {
         allproductlistFromDb =
             ProductRepository.getItemsdetails(requireContext)
         return allproductlistFromDb
     }*/
    fun getAllproducts(
        requireContext: Context
    ): LiveData<List<ItemTable>?>? {
        allproductlistFromDb =
            ProductRepository.getItemsdetails(requireContext)
        return allproductlistFromDb
    }


    /*suspend fun getproductsbycategory(
        requireContext: Context,
        categoryId: Int?
    ): List<ItemTable>? {
        productlistFromDbbycategory =
            ProductRepository.getproductsbycategory(requireContext, categoryId)
        return productlistFromDbbycategory
    }*/
    fun getproductsbycategory(
        requireContext: Context,
        categoryId: Int?
    ): LiveData<List<ItemTable>?>? {
        productlistFromDbbycategory =
            ProductRepository.getproductsbycategory(requireContext, categoryId)
        return productlistFromDbbycategory
    }


    //------ CART------
    suspend fun addToCartApiCall(
        item: ItemTable,
        requireContext: Context,
        seletedcustomer: String,
        selected_customerId: Int,
        charged_amount: String
    ) {
        CartRespository.inserCartItem(requireContext, item, seletedcustomer, selected_customerId,charged_amount)

    }

    private suspend fun insertToCartDb(requireContext: Context, cartDaoModel: CartItemTable) {
        CartRespository.insertData(requireContext, cartDaoModel)

    }


    fun getAllCartitems(requireContext: Context): LiveData<List<CartItemTable>?>? {
        cartItemListFromDb = CartRespository.getAllCartItems(requireContext)
        return cartItemListFromDb
    }

    fun removerFromCart(
        requireContext: Context,
        cart_id: Int?,
        cartQty: Int?,
        price: Double?,
        total: Double?
    ) {
        CartRespository.database!!.getDao().deleteByItemId(cart_id!!)
        var tot = (price!! * cartQty!!)
        var newtotal = total!!.minus(tot)
        Log.d("TAG", "removerFromCart: "+newtotal)
        CartRespository.multicarttotalChargeUpdate(requireContext,newtotal)
    }

    fun getAllCustomers() {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = App().retrofit.getCustomerList()

                Log.d("ViewModel", "getCustomers: " + response)

                if (response.status.equals("success")) {
                    _getAllCustomers.value = response
                } else {
                    _getAllCustomers.value = response
                }

            } catch (e: Exception) {
                Log.d("Exception", "getAllCustomers: ")
                _getAllCustomers.value =
                    CustomerListResponse(message = "Something went wrong", status = "error")
            }
        }
    }

    fun updateCustomer(context: Context, selectedCustomerid: Int, customername: String) {
        CartRespository.updatecustomer(context, selectedCustomerid, customername)
    }

    fun quantityUpdate(context: Context, selectedCartid: Int?, newqtyy: String) {
        CartRespository.updateQuantity(context, selectedCartid, newqtyy)
    }

    fun discountUpdate(context: Context, selectedCartid: Int?, newdiscount: String) {
        CartRespository.updateDiscount(context, selectedCartid, newdiscount)
    }

    fun totalChargeUpdate(context: Context, chargedAmount: Double) {
        CartRespository.totalChargeUpdate(context,chargedAmount)
    }


}