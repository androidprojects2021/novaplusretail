package com.novaplus.retailer.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.novaplus.retailer.R
import com.novaplus.retailer.database.tables.MulticartTable

class MulticartAdapter(
    val context: Context, var multicartlist: List<MulticartTable>?,
    val multicartinterface: MultiCartInterface, val itemSelected: (MulticartTable?) -> Unit
) :
    RecyclerView.Adapter<MulticartAdapter.PlaceViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MulticartAdapter.PlaceViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_orderlist, parent, false)
        return PlaceViewHolder(view)
    }

    override fun onBindViewHolder(holder: MulticartAdapter.PlaceViewHolder, position: Int) {
        Log.d("TAG", "onBindViewHolder: " + multicartlist?.get(position)?.status)
        when (multicartlist?.get(position)?.status) {
            "OnHold" -> {
                holder.img_delete.visibility = View.VISIBLE
                holder.img_print.visibility = View.GONE

                holder.ll_multicart.setOnClickListener {
                    multicartinterface.clickItem(multicartlist?.get(position))
                }
            }
            "Finished" -> {
                holder.img_print.visibility = View.VISIBLE
                holder.img_delete.visibility = View.GONE
            }
            else -> {
                holder.img_delete.visibility = View.GONE
                holder.img_print.visibility = View.GONE
            }
        }
        holder.txt_orderdate.text = multicartlist?.get(position)?.order_date
        holder.txt_invoiceid.text = multicartlist?.get(position)?.invoice_id
        holder.txt_customername.text = multicartlist?.get(position)?.customer_name
       // holder.txt_employeename.text = multicartlist?.get(position)?.employee
        holder.txt_total.text = multicartlist?.get(position)?.total.toString()
        holder.txt_status.text = multicartlist?.get(position)?.status

        holder.img_delete.setOnClickListener {
            itemSelected.invoke(multicartlist?.get(position))
        }
        holder.img_print.setOnClickListener {
            multicartinterface.receiptItemClick(multicartlist?.get(position))
        }

    }

    override fun getItemCount(): Int {
        return multicartlist!!.size
    }

    fun refreshdata(multicartList: ArrayList<MulticartTable>?) {
        this.multicartlist = multicartList
        notifyDataSetChanged()
        Log.d("TAG", "refreshdata: " + this.multicartlist)
    }

    inner class PlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txt_orderdate = itemView.findViewById<TextView?>(R.id.txt_orderdate)
        val txt_invoiceid = itemView.findViewById<TextView?>(R.id.txt_invoiceid)
        val txt_customername = itemView.findViewById<TextView?>(R.id.txt_customername)
        val txt_employeename = itemView.findViewById<TextView?>(R.id.txt_employeename)
        val txt_total = itemView.findViewById<TextView?>(R.id.txt_total)
        val txt_status = itemView.findViewById<TextView?>(R.id.txt_status)
        val img_delete = itemView.findViewById<ImageView?>(R.id.img_delete)
        val img_print = itemView.findViewById<ImageView?>(R.id.img_print)
        val ll_multicart = itemView.findViewById<ConstraintLayout?>(R.id.ll_multicart)
    }

    interface MultiCartInterface {
        fun clickItem(get: MulticartTable?)
        fun receiptItemClick(get: MulticartTable?)
    }

}