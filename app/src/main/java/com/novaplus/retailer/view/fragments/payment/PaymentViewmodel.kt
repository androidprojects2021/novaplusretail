package com.novaplus.retailer.view.fragments.payment

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.model.input.order.InputOrderItems
import com.azinova.model.response.customers_list.CustomerListResponse
import com.azinova.model.response.order.ResponseOrderItems
import com.azinova.model.response.order.Result
import com.novaplus.retailer.App
import com.novaplus.retailer.database.NovaplusDatabase
import com.novaplus.retailer.database.repository.CartRespository
import com.novaplus.retailer.database.tables.CartItemTable
import kotlinx.coroutines.*
import java.lang.Exception
import java.text.SimpleDateFormat

@InternalCoroutinesApi
class PaymentViewmodel:ViewModel() {
    var database: NovaplusDatabase? = null

    var cartItemListFromDb: LiveData<List<CartItemTable>?>? = null

    private val _createorderStatus = MutableLiveData<ResponseOrderItems>()
    val createorderStatus: LiveData<ResponseOrderItems>
        get() = _createorderStatus

    private val _getAllCustomers = MutableLiveData<CustomerListResponse>()
    val getAllCustomers: LiveData<CustomerListResponse>
        get() = _getAllCustomers

    fun getAllCartitems(requireContext: Context): LiveData<List<CartItemTable>?>? {
        cartItemListFromDb = CartRespository.getAllCartItems(requireContext)
        return cartItemListFromDb
    }


    fun paymentValidate(
        context: Context,
        inputorder: InputOrderItems,
        cartlist: ArrayList<CartItemTable>?
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = App().retrofit.CreateOrder(inputorder)

                Log.d("ViewModel", "createorder: " + response)

                if (response.result?.status.equals("success")) {
                    _createorderStatus.value = response
                   clearCart(context)
                    addToMulticart(context,cartlist,response.result)
                } else {
                    _createorderStatus.value = response
                }

            } catch (e: Exception) {
                Log.d("Exception", "Payment: "+e.localizedMessage)
                _createorderStatus.value =
                    ResponseOrderItems(Result(message = "Something went wrong", status = "error") )

            }
        }
    }

    private suspend fun addToMulticart(
        context: Context,
        cartlist: ArrayList<CartItemTable>?,
        result: Result?
    ) {
        //"2022-04-22 15:30:23"
        var spf = SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
        val new = spf.parse(result?.order_date)
        spf = SimpleDateFormat("dd-mm-yyyy")
        val orderdate: String = spf.format(new)

        val newcount = App().sharedPrefs.cartcount.plus(1)
        App().sharedPrefs.cartcount = newcount
        Log.d("NewCartCount ", "onViewCreated: " + newcount)
        CartRespository.addMulticartItem(context, cartlist!!,newcount,"Finished",orderdate,result?.invoice_id.toString(),result?.invoice.toString())
    }

    private suspend fun clearCart(context: Context) {
        CartRespository.clearCart(context)
    }

    fun getAllCustomers() {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = App().retrofit.getCustomerList()

                Log.d("ViewModel", "getCustomers: " + response)

                if (response.status.equals("success")) {
                    _getAllCustomers.value = response
                } else {
                    _getAllCustomers.value = response
                }

            } catch (e: Exception) {
                Log.d("Exception", "getAllCustomers: ")
                _getAllCustomers.value =
                    CustomerListResponse(message = "Something went wrong", status = "error")
            }
        }
    }

    fun updateCustomer(context: Context, selectedCustomerid: Int, customername: String) {
        CartRespository.updatecustomer(context, selectedCustomerid, customername)
    }
}