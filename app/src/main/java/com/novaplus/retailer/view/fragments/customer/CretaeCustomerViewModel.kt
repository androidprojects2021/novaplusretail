package com.novaplus.retailer.view.fragments.customer

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.model.input.InputCreateCustomer
import com.azinova.model.response.create_customer.CreateCustomerResponse
import com.azinova.model.response.create_customer.Result
import com.azinova.model.response.customers_list.CustomerListResponse
import com.azinova.model.response.customers_list.CustomersItem
import com.novaplus.retailer.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class CretaeCustomerViewModel : ViewModel() {



    private val _createCustomerstatus = MutableLiveData<CreateCustomerResponse>()
    val createCustomerstatus: LiveData<CreateCustomerResponse>
        get() = _createCustomerstatus

    var getcustomersfromdb: List<CustomersItem>? = null

    fun addcustomer(inputCreateCustomer: InputCreateCustomer) {
        GlobalScope.launch(Dispatchers.Main){
            try {

                val response = App().retrofit.createCustomerApi(inputCreateCustomer)
                if (response.result?.status.equals("success")) {
                    _createCustomerstatus.value = response

                } else {
                    _createCustomerstatus.value = response
                }
            } catch (e: Exception) {
                _createCustomerstatus.value = CreateCustomerResponse(
                    Result(
                        message = "Something went wrong",
                        status = "Error"
                    )
                )

            }
        }

    }

}