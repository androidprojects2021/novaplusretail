package com.novaplus.retailer.view.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.azinova.barcodeprinter.utils.ThreadPoolmanager
import com.novaplus.retailer.R
import com.novaplus.retailer.view.SunmiScanner
import woyou.aidlservice.jiuiv5.IWoyouService
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class ScanPrintActivity : AppCompatActivity() {

    private val TAG: String =ScanPrintActivity::class.java.getSimpleName()
    lateinit var  buttonPrint: Button

    private val SERVICE_PACKAGE = "woyou.aidlservice.jiuiv5"
    private val SERVICE_ACTION = "woyou.aidlservice.jiuiv5.IWoyouService"
    private var woyouService: IWoyouService? = null
    private var sunmiScanner: SunmiScanner? = null

    private var  ACTION_DATA_CODE_RECEIVED ="com.sunmi.scanner.ACTION_DATA_CODE_RECEIVED";
    private var   DATA = "data";

    private var company_name: String? = null
    private var company_logo: String? = null
    private var secret_code: String? = null
    private var instructions: String? = null

    private var transaction_id: String? = null
    private var date_of_purchase: String? = null
    private var validity: String? = null
    private var amount: String? = null
    private var date: String? = null
    private var validity_date: String? = null
    private var total_amount: String? = null
    private var time: String? = null
    private var date_purchase: String? = null

    private var providerName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sacn_print)

//        buttonPrint = findViewById(R.id.buttonPrint)

        connectPrinterService()
        initPrinter()

        onClick()

        company_name = getIntent().getStringExtra("company_name")
        company_logo = getIntent().getStringExtra("company_logo")
        instructions = getIntent().getStringExtra("instructions")
        secret_code =getIntent().getStringExtra("secret_code")
        transaction_id = getIntent().getStringExtra("trenascation_id")
        date_of_purchase = getIntent().getStringExtra("date_of_purchase")
        validity =  getIntent().getStringExtra("validity")
        providerName =getIntent().getStringExtra("providerName")
        amount = getIntent().getIntExtra("amount", 0).toString()
//
//        txt_secret_code.text = secret_code
//        txt_instructions.text = instructions
//        txt_company_name.text = company_name
//
//
//        txt_purchase.text =    getDate(date_of_purchase)
//        date_purchase  = getDate(date_of_purchase)
//
//
//        txt_validity_date.text =  getDate(validity)
//        validity_date = getDate(validity)
//
//        txt_amount.text = "AED "  + amount
//        total_amount = "AED "  + amount
//
//        date = getDateFormatFromDate(date_of_purchase)
//        time = getTimeInFormatFromDateAndTime(date)
//        txt_time.text = time!!
//
//        Glide.with(this).load(company_logo).error(R.drawable.app_icon).into(img_company_image)

    }

    private fun onClick() {

        buttonPrint.setOnClickListener(View.OnClickListener {
            printFunction(secret_code, instructions, company_name, company_logo, date_purchase,time!!,validity_date!!, total_amount)
        })

//        img_back_print_screen.setOnClickListener(View.OnClickListener {
//            findNavController(R.id.finalEstimateFragment)
//        })
    }


    private fun printFunction(secretCode: String?, instructions: String?, companyName: String?, companyLogo: String?, date_purchase: String?,time:String, validity_date: String?, total_amount: String?) {
        ThreadPoolmanager.executeTask(Runnable {
            try {

//                val l1 = arrayOf("Provider Name    :", "$providerName")
                val l1= arrayOf("Company Name     :", "$companyName")
                val l2 = arrayOf("Date of Purchase :", "$date_purchase")
                val l3 = arrayOf("Time of Purchase :", "$time")
                val l4 = arrayOf("Voucher          :", "$secretCode")
                val l5 = arrayOf("Validity         :", "$validity_date")
                val l6 = arrayOf("Amount           :", "$total_amount")


//                val mBitmap = BitmapFactory.decodeResource(resources, R.drawable.blue_plus_white_logo)

//                val mBitmapAppIcon = BitmapFactory.decodeResource(resources, R.drawable.blue_app_black_white_icon)


                woyouService!!.setAlignment(1, null)
//                woyouService!!.printBitmap(scaleImage(mBitmap), null)
                woyouService!!.printTextWithFont(" \n", "", 30f, null)

                woyouService!!.printTextWithFont("Bring things closer and make\n life easier, like never!", "gh", 20f, null)

                woyouService!!.printTextWithFont(" \n", "", 30f, null)

                woyouService!!.printTextWithFont(" \n", "", 30f, null)


                woyouService!!.printTextWithFont(providerName, "gh", 25f, null)
                woyouService!!.printTextWithFont(" \n", "", 30f, null)
//                woyouService!!.printTextWithFont(" \n", "", 30f, null)

                woyouService?.setFontSize(18f, null)
                woyouService?.printColumnsText(l1, intArrayOf(22, 15), intArrayOf(0, 0), null)
                woyouService?.printColumnsText(l2, intArrayOf(22, 15), intArrayOf(0, 0), null)
                woyouService?.printColumnsText(l3, intArrayOf(22, 15), intArrayOf(0, 0), null)
                woyouService?.printColumnsText(l4, intArrayOf(22, 15), intArrayOf(0, 0), null)
                woyouService?.printColumnsText(l5, intArrayOf(22, 15), intArrayOf(0, 0), null)
                woyouService?.printColumnsText(l6, intArrayOf(22, 15), intArrayOf(0, 0), null)

                woyouService?.printTextWithFont("--------------------------------------\n", "", 20f, null)

                woyouService!!.printTextWithFont(" \n", "", 30f, null)

                woyouService?.printTextWithFont(instructions, "", 20f, null)

                woyouService!!.printTextWithFont(" \n", "", 30f, null)
                woyouService!!.printTextWithFont(" \n", "", 30f, null)


                woyouService!!.printTextWithFont("********Thank You For Purchase********", "gh", 20f, null)

                woyouService!!.printTextWithFont(" \n", "", 30f, null)
                woyouService!!.printTextWithFont(" \n", "", 30f, null)

                woyouService?.setFontSize(20f, null)

                woyouService!!.lineWrap(3, null)

            } catch (e: Exception) {

            }
        })

    }

    private fun scaleImage(bitmap1: Bitmap): Bitmap? {
        val width = bitmap1.width
        val height = bitmap1.height
        val newWidth = (width / 8 + 1) * 6
        val scaleWidth = newWidth.toFloat() / width
        val matrix = Matrix()
        matrix.postScale(scaleWidth, 1f)
        return Bitmap.createBitmap(bitmap1, 0, 0, width, height, matrix, true)
    }


    fun initPrinter() {
        if (woyouService == null) {
            return
        }
        try {
            woyouService!!.printerInit(null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }
    private fun connectPrinterService() {
        val intent = Intent()
        intent.setPackage(this.SERVICE_PACKAGE)
        intent.setAction(this.SERVICE_ACTION)
        application.startService(intent)
        application.bindService(intent, connService, Context.BIND_AUTO_CREATE)

    }
    private val connService: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            woyouService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            woyouService = IWoyouService.Stub.asInterface(service)
        }
    }



    fun getDateandTime(unformated_date: String?): String? {

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val calendar = Calendar.getInstance()
        var returnDate: String
        val monthNames = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec")
        try {
            val date = simpleDateFormat.parse(unformated_date)
            calendar.time = date
            val AM_PM = calendar[Calendar.AM_PM]
            val am: String
            am = if (AM_PM == 0) {
                "AM"
            } else {
                "PM"
            }
            calendar.timeZone = TimeZone.getDefault()
            returnDate = monthNames[calendar[Calendar.MONTH]] + " " + calendar[Calendar.DAY_OF_MONTH] + ", " + calendar[Calendar.YEAR]
        } catch (e: ParseException) {
            e.printStackTrace()
            returnDate = ""
        }
        return returnDate
    }




    fun getDateFormatFromDate(dateandtime: String?): String? {

        var localdate: String = ""

        try {
            val utcFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            utcFormat.setTimeZone(TimeZone.getTimeZone("GMT"))

            val date: Date = utcFormat.parse(dateandtime)

            val deviceFormat: DateFormat = SimpleDateFormat("dd MMM yyyy hh:mm aaa")

            deviceFormat.setTimeZone(TimeZone.getDefault()) //Device timezone
            localdate = deviceFormat.format(date)

        } catch (e: Exception) {
            return localdate
        }
        return localdate
    }



    fun getTimeInFormatFromDateAndTime(dateandtime: String?): String? {
        var localdate = ""
        val format = SimpleDateFormat("dd MMM yyyy hh:mm aaa")
        format.timeZone = TimeZone.getTimeZone("UTC")
        val format2 = SimpleDateFormat("hh:mm a")
        format2.timeZone = TimeZone.getTimeZone("GMT")
        var date: Date? = null
        try {
            date = format.parse(dateandtime)
            localdate = format2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            return localdate
        }
        return localdate
    }




    fun getDate(dateandtime: String?): String? {

        var localdate: String = ""

        try {
            val utcFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            utcFormat.setTimeZone(TimeZone.getTimeZone("GMT"))

            val date: Date = utcFormat.parse(dateandtime)

            val deviceFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")

            deviceFormat.setTimeZone(TimeZone.getDefault()) //Device timezone
            localdate = deviceFormat.format(date)

        } catch (e: Exception) {
            return localdate
        }
        return localdate
    }


}