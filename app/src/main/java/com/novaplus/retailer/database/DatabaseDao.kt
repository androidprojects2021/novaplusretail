package com.novaplus.retailer.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.database.tables.CategoryTable
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.database.tables.MulticartTable


@Dao

interface DatabaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCategorys(categoryTable: CategoryTable)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllItems(itemTable: ItemTable)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCartItems(cartTable: CartItemTable)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllToMultiCart(multicartTable: MulticartTable)


    @Query("SELECT * FROM category_table")
    fun getAllcategorys(): LiveData<List<CategoryTable>?>?

    @Query("SELECT * FROM item_table")
    fun getAllItems(): LiveData<List<ItemTable>?>?

    @Query("SELECT * FROM item_table")
    fun getallItems(): List<ItemTable>?

    @Query("SELECT * FROM item_table WHERE category=:category")
    fun getallitemsBycategory(category: String): LiveData<List<ItemTable>?>?

    @Query("SELECT * FROM cart_table")
    fun getAllCartItems(): LiveData<List<CartItemTable>?>?

    @Query("SELECT * FROM cart_table")
    fun getAllCartItem(): List<CartItemTable>?

    @Query("SELECT * FROM multicart_table WHERE multicart_id=:multicart_id ORDER BY order_date ASC")
    fun getallMulticartlist(multicart_id : Int): LiveData<List<MulticartTable>?>?

    @Query("SELECT DISTINCT multicart_id FROM MULTICART_TABLE ORDER BY order_date ASC")
    fun getMulticartlist(): LiveData<List<Int>?>?

    @Query("SELECT DISTINCT multicart_id FROM MULTICART_TABLE WHERE status=:status ORDER BY order_date ASC")
    fun getMulticartCount(status: String): LiveData<List<Int>?>?



    @Query("UPDATE cart_table SET customer_id=:customer_id ,customer_name=:customername ")
    fun updatecartcustomer(customer_id: Int, customername: String)


    @Query("UPDATE cart_table SET cartQty=:cartQty WHERE cart_id=:selectedCartid ")
    fun updateQuantity(selectedCartid: Int?, cartQty: Int)

    @Query("UPDATE cart_table SET discount=:newdiscount ")
    fun updateDiscount(newdiscount: Int)

    @Query("UPDATE cart_table SET total=:total")
    fun totalChargeUpdate(total: Double)

    @Query("UPDATE multicart_table SET total=:total")
    fun multicarttotalChargeUpdate(total: Double)


    @Query("DELETE FROM cart_table WHERE cart_id = :cart_id")
    fun deleteByItemId(cart_id: Int)

    @Query("DELETE FROM multicart_table WHERE multicart_id = :multicart_id")
    fun multicartdeleteById(multicart_id: Int)

    @Query("DELETE FROM category_table")
    suspend fun deleteCategorys()

    @Query("DELETE FROM item_table")
    suspend fun deleteitems()

    @Query("DELETE FROM cart_table")
    suspend fun deleteCartItems()

    @Query("DELETE FROM MULTICART_TABLE")
    suspend fun deleteMulticartItems()


}