package com.novaplus.retailer.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "customer_table")
data class CustomerTable(
    @PrimaryKey
    @ColumnInfo(name = "id")
    var customer_id: Int? = null,

    @ColumnInfo(name = "name")
    var customer_name: String? = null,

    @ColumnInfo(name = "phone")
    var customer_landphone: String? = null,

    @ColumnInfo(name = "mobile")
    var customer_mobile: String? = null,

    @ColumnInfo(name = "email")
    var customer_email: String? = null


)
