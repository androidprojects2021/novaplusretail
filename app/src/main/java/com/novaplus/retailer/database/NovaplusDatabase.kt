package com.novaplus.retailer.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.database.tables.CategoryTable
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.database.tables.MulticartTable
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized


@Database(entities = arrayOf(CategoryTable :: class, ItemTable :: class, CartItemTable :: class,MulticartTable :: class), version = 1, exportSchema = false)
abstract class NovaplusDatabase : RoomDatabase() {

    abstract fun getDao(): DatabaseDao

    @InternalCoroutinesApi
    companion object {

        @Volatile
        private var INSTANCE: NovaplusDatabase? = null

        fun getDataseClient(context: Context): NovaplusDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, NovaplusDatabase::class.java, "APP_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()


                return INSTANCE!!

            }
        }



    }



}


/*// db Migration
       // upgrade db version and add below code

       INSTANCE =  Room
       .databaseBuilder(context, EmaidDatabase::class.java, "APP_DATABASE")
       .addMigrations(MIGRATION_1_2)
       .build()
       val MIGRATION_1_2 = object : Migration(1, 2) {
           override fun migrate(database: SupportSQLiteDatabase) {
               database.execSQL("ALTER TABLE   ADD COLUMN  ")
               Log.d("TAG", "migrate: ")

           }
       }*/
