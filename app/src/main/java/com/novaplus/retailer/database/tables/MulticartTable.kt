package com.novaplus.retailer.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "multicart_table")
data class MulticartTable(

    @PrimaryKey(autoGenerate = true)
    var _id: Int? = null,

    @ColumnInfo(name = "id")
    var item_id: Int? = null,

    @ColumnInfo(name = "multicart_id")
    var multicart_id: Int? = null,

    @ColumnInfo(name = "name")
    var item_name: String? = null,

    @ColumnInfo(name = "employee")
    var employee: String? = null,

    @ColumnInfo(name = "order_date")
    var order_date: String? = null,

    @ColumnInfo(name = "price")
    var price: Double? = null,

    @ColumnInfo(name = "cost")
    var cost: Double? = null,

    @ColumnInfo(name = "tax")
    var tax: Int? = null,

    @ColumnInfo(name = "cartQty")
    var cartQty: Int? = null,

    @ColumnInfo(name = "discount")
    var discount: Int? = 0,

    @ColumnInfo(name = "customer_id")
    var customer_id: Int? = null,

    @ColumnInfo(name = "customer_name")
    var customer_name: String? = null,

    @ColumnInfo(name = "qty_available")
    var qty_available: Int? = null,

    @ColumnInfo(name = "total")
    var total: Double? = null,

    @ColumnInfo(name = "status")
    var status: String? = null,

    @ColumnInfo(name = "invoice_id")
    var invoice_id: String? = null,

    @ColumnInfo(name = "invoice")
    var invoice: String? = null
)
