package com.novaplus.retailer.database.repository

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.novaplus.retailer.App
import com.novaplus.retailer.database.NovaplusDatabase
import com.novaplus.retailer.database.tables.CartItemTable
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.database.tables.MulticartTable
import com.novaplus.retailer.view.activity.BottomHostActivity
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class CartRespository {

    companion object {
        var database: NovaplusDatabase? = null

        //var cartItemlistFromDb: List<CartItemTable>? = null
        var cartItemlistFromDb: LiveData<List<CartItemTable>?>? = null
        var cartItemslistFromDb: List<CartItemTable>? = null

        var bmulticartcountFromDb: LiveData<List<Int>?>? = null

        var multicartlistFromDb: LiveData<List<MulticartTable>?>? = null
        //var multicartlistFromDb: List<MulticartTable>? = null


        var multicartlistByIdFromDb: LiveData<List<Int>?>? = null

        var allmulticartlistFromDb: LiveData<List<Int>?>? = null

        fun initializeDB(context: Context): NovaplusDatabase {
            return NovaplusDatabase.getDataseClient(context)
        }

        suspend fun insertData(requireContext: Context, cartDaoModel: CartItemTable) {
            database = initializeDB(requireContext)

            /*try {
                for (cart_item in cartItemlistFromDb!!) {
                    val cart_details = CartItemTable(
                        item_id = cart_item.item_id,
                        item_name = cart_item?.item_name,
                        price = cart_item.price,
                        cost = cart_item.cost,
                        cartQty = cart_item.cartQty
                    )

                    Log.e("Insert - cart_details", "Insert: $cart_details")

                    database!!.getDao().insertAllCartItems(cart_details)
                }

            } catch (e: Exception) {
            }*/
        }


        /* @InternalCoroutinesApi
         suspend fun getAllCartItems(requireContext: Context): List<CartItemTable>? {
             database = initializeDB(requireContext)
             cartItemlistFromDb = database!!.getDao().getAllCartItems()
             return cartItemlistFromDb
         }*/
        fun getAllCartItems(requireContext: Context): LiveData<List<CartItemTable>?>? {
            database = initializeDB(requireContext)
            cartItemlistFromDb = database!!.getDao().getAllCartItems()
            return cartItemlistFromDb
        }

        fun getCartItems(requireContext: Context): List<CartItemTable>? {
            database = initializeDB(requireContext)
            cartItemslistFromDb = database!!.getDao().getAllCartItem()
            return cartItemslistFromDb
        }

        @SuppressLint("LongLogTag")
        suspend fun inserCartItem(
            requireContext: Context,
            item: ItemTable,
            seletedcustomer: String,
            selected_customerId: Int,
            charged_amount: String
        ) {

            database = initializeDB(requireContext)
            try {
                val cartitem = CartItemTable(
                    item_id = item.item_id,
                    item_name = item.item_name,
                    price = item.price,
                    cost = item.cost,
                    tax = item.tax,
                    cartQty = 1,
                    discount = 0,
                    customer_id = selected_customerId,
                    customer_name = seletedcustomer,
                    qty_available = item.qty_available,
                    total = charged_amount.toDouble()
                )
                Log.e("Insert - cartitemdetails", "Insert: $cartitem")
                database!!.getDao().insertAllCartItems(cartitem)

            } catch (e: Exception) {
                Log.d("Exception", "inserCartItem: " + e.localizedMessage)
            }

        }

        fun updatecustomer(context: Context, selectedCustomerid: Int, customername: String) {
            database = initializeDB(context)
            database!!.getDao().updatecartcustomer(selectedCustomerid, customername)
        }

        fun updateQuantity(context: Context, selectedCartid: Int?, qty: String) {
            database = initializeDB(context)
            database!!.getDao().updateQuantity(selectedCartid, qty.toInt())
        }

        fun updateDiscount(context: Context, selectedCartid: Int?, newdiscount: String) {
            database = initializeDB(context)
            database!!.getDao().updateDiscount(newdiscount.toInt())
        }

        suspend fun addMulticartItem(
            context: Context,
            newcartlist: List<CartItemTable>,
            newcount: Int,
            Status: String,
            date: String,
            invoice_id: String,
            invoice: String
        ) {


            database = initializeDB(context)
            try {
                for (cartitem in newcartlist) {
                    val multicartitem = MulticartTable(
                        item_id = cartitem.item_id,
                        multicart_id = newcount,
                        item_name = cartitem.item_name,
                        price = cartitem.price,
                        cost = cartitem.cost,
                        tax = cartitem.tax,
                        cartQty = cartitem.cartQty,
                        discount = cartitem.discount,
                        customer_id = cartitem.customer_id,
                        customer_name = cartitem.customer_name,
                        qty_available = cartitem.qty_available,
                        employee = App().sharedPrefs._name,
                        order_date = date,
                        total = cartitem.total,
                        status = Status,
                        invoice_id = invoice_id,
                        invoice = invoice

                    )

                    Log.e("Insert - multicartitemdetails", "Insert: $multicartitem")
                    database!!.getDao().insertAllToMultiCart(multicartitem)
                    database!!.getDao().deleteCartItems()
                }
            } catch (e: Exception) {
                Log.d("Exception", "addMulticartItem: ")
            }
        }

        fun getmulticartcount(
            bottomHostActivity: BottomHostActivity,
            status: String
        ): LiveData<List<Int>?>? {
            database = initializeDB(bottomHostActivity)
            bmulticartcountFromDb = database!!.getDao().getMulticartCount(status)
            return bmulticartcountFromDb
        }

        suspend fun clearCart(context: Context) {
            database = initializeDB(context)
            database!!.getDao().deleteCartItems()
        }

        fun getallMulticartlist(context: Context, cart_id: Int): LiveData<List<MulticartTable>?>? {
            database = initializeDB(context)
            multicartlistFromDb = database!!.getDao().getallMulticartlist(cart_id)
            return multicartlistFromDb
        }

        fun getallMulticartlistById(context: Context, status: String): LiveData<List<Int>?>? {
            database = initializeDB(context)
            multicartlistByIdFromDb = database!!.getDao().getMulticartCount(status)
            return multicartlistByIdFromDb
        }

        fun getallMulticartlist(context: Context): LiveData<List<Int>?>? {
            database = initializeDB(context)
            allmulticartlistFromDb = database!!.getDao().getMulticartlist()
            return allmulticartlistFromDb
        }

        fun totalChargeUpdate(context: Context, chargedAmount: Double) {
            database = initializeDB(context)
            database!!.getDao().totalChargeUpdate(chargedAmount)
        }

        fun multicarttotalChargeUpdate(context: Context, chargedAmount: Double) {
            database = initializeDB(context)
            database!!.getDao().multicarttotalChargeUpdate(chargedAmount)
        }

    }
}