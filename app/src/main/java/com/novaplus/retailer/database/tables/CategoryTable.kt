package com.novaplus.retailer.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "category_table")
data class CategoryTable(

    @PrimaryKey
    @ColumnInfo(name = "id")
    var category_id: Int? = null,

    @ColumnInfo(name = "name")
    var category_name: String? = null


)
