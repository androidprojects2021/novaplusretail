package com.novaplus.retailer.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "item_table")
data class ItemTable(

    @PrimaryKey
    @ColumnInfo(name = "id")
    var item_id: Int? = null,

    @ColumnInfo(name = "name")
    var item_name: String? = null,

    @ColumnInfo(name = "category")
    var category_id: String? = null,

    @ColumnInfo(name = "type")
    var type: String? = null,

    @ColumnInfo(name = "price")
    var price: Double? = null,

    @ColumnInfo(name = "cost")
    var cost: Double? = null,

    @ColumnInfo(name = "code")
    var code: String? = null,

    @ColumnInfo(name = "image_url")
    var imageUrl: String? = null,

    @ColumnInfo(name = "tax")
    var tax: Int? = null,

    @ColumnInfo(name = "barcode")
    var barcode: String? = null,

    @ColumnInfo(name = "qty_available")
    var qty_available: Int? = null


)
