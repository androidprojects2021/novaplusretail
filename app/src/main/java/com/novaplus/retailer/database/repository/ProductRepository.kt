package com.novaplus.retailer.database.repository

import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import com.azinova.model.response.customers_list.CustomersItem
import com.azinova.model.response.data.CategoriesItem
import com.azinova.model.response.data.ProductsItem
import com.novaplus.retailer.database.NovaplusDatabase
import com.novaplus.retailer.database.tables.CategoryTable
import com.novaplus.retailer.database.tables.CustomerTable
import com.novaplus.retailer.database.tables.ItemTable
import com.novaplus.retailer.view.activity.BottomHostActivity
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.Exception
import java.net.URL
import java.util.*
@InternalCoroutinesApi
class ProductRepository {

    companion object {
        var database: NovaplusDatabase? = null

        var itemslistFromDb: List<ItemTable>? = null

        var itemlistFromDb: LiveData<List<ItemTable>?>? = null

        //var categorylistFromDb: List<CategoryTable>? = null
        var categorylistFromDb: LiveData<List<CategoryTable>?>? = null

        //var productlistFromDbbycategory: List<ItemTable>? = null
        var productlistFromDbbycategory: LiveData<List<ItemTable>?>? = null

        var allproductlistFromDb: List<ItemTable>? = null
        var allcustomersFromDb: List<CustomersItem>? = null


        fun initializeDB(context: Context): NovaplusDatabase {
            return NovaplusDatabase.getDataseClient(context)
        }

        @DelicateCoroutinesApi
        @SuppressLint("LongLogTag")
        suspend fun insertData(context: Context, categories: ArrayList<CategoriesItem?>?, products: ArrayList<ProductsItem?>?) {
            var image = ""
            database = initializeDB(context)

            database!!.getDao().deleteCategorys()
            database!!.getDao().deleteitems()


            try {
                if (!categories.isNullOrEmpty()) {
                    for (catagoryitem in categories) {
                        val categorydetails = CategoryTable(category_id = catagoryitem?.id, category_name = catagoryitem?.name)
                        Log.e("Insert - categorydetails", "Insert: $categorydetails")
                        database!!.getDao().insertAllCategorys(categorydetails)
                    }
                }
                if (!products.isNullOrEmpty()) {
                    for (productitem in products) {


                        if (!productitem?.imageUrl.isNullOrEmpty()) {
                            Log.d("Image Url 1", "insertData Image: " + productitem?.imageUrl)
                            val urlImage: URL = URL(productitem?.imageUrl)

                            val result: Deferred<Bitmap?> = GlobalScope.async {
                                urlImage.toBitmap()
                            }

                            val bitmap: Bitmap? = result.await()

                            bitmap?.apply {
                                val savedUri: Uri? = saveToInternalStorage(context)
                                Log.e(
                                    "productimage",
                                    "doLogin: " + savedUri.toString()
                                )
                                image = savedUri.toString()
                            }


                        } else {
                            image = ""
                        }


                        val productdetails = ItemTable(
                            item_id = productitem?.id,
                            item_name = productitem?.name,
                            category_id = productitem?.category.toString(),
                            type = productitem?.type,
                            price = productitem?.price,
                            cost = productitem?.cost,
                            code = productitem?.code,
                            imageUrl = image,
                            tax = productitem?.tax,
                            barcode = productitem?.barcode,
                            qty_available = productitem?.quantity
                        )
                        Log.e("Insert - productdetails", "Insert: $productdetails")
                        database!!.getDao().insertAllItems(productdetails)
                    }
                }


            } catch (e: Exception) {
                Log.e("Exception", "Insertschedule&Maid: ${e.message}")
            }
        }


        // --------------------------Image Download -----------------
        fun URL.toBitmap(): Bitmap? {
            return try {
                BitmapFactory.decodeStream(openStream())
            } catch (e: IOException) {
                null
            }
        }

        fun Bitmap.saveToInternalStorage(context: Context): Uri? {
            val wrapper = ContextWrapper(context)

            var file = wrapper.getDir("product_images", Context.MODE_PRIVATE)

            file = File(file, "${UUID.randomUUID()}.jpg")

            return try {
                val stream: OutputStream = FileOutputStream(file)

                compress(Bitmap.CompressFormat.JPEG, 100, stream)

                stream.flush()

                stream.close()

                Uri.parse(file.absolutePath)
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }
        }

// -------------------------------------------------------------------------


        /*@InternalCoroutinesApi
        suspend fun getallItemsdetails(context: Context): List<ItemTable>? {
            database = initializeDB(context)
            itemlistFromDb = database!!.getDao().getAllItems()
            return itemlistFromDb
        }*/


        fun getItemsdetails(context: Context): LiveData<List<ItemTable>?>? {
            database = initializeDB(context)
            itemlistFromDb = database!!.getDao().getAllItems()
            return itemlistFromDb
        }


      /*  @InternalCoroutinesApi
        suspend fun getAllCategories(requireContext: Context): List<CategoryTable>? {
            database = initializeDB(requireContext)
            categorylistFromDb = database!!.getDao().getAllcategorys()
            return categorylistFromDb
        } */



         fun getAllCategories(requireContext: Context): LiveData<List<CategoryTable>?>? {
            database = initializeDB(requireContext)
            categorylistFromDb = database!!.getDao().getAllcategorys()
            return categorylistFromDb
        }

        /*@InternalCoroutinesApi
        suspend fun getproductsbycategory(
            requireContext: Context,
            categoryId: Int?
        ): List<ItemTable>? {
            database = initializeDB(requireContext)
            productlistFromDbbycategory =
                database!!.getDao().getallitemsBycategory(categoryId.toString())
            return productlistFromDbbycategory
        }*/



         fun getproductsbycategory(
            requireContext: Context,
            categoryId: Int?
        ): LiveData<List<ItemTable>?>? {
            database = initializeDB(requireContext)
            productlistFromDbbycategory =
                database!!.getDao().getallitemsBycategory(categoryId.toString())
            return productlistFromDbbycategory
        }


        suspend fun clearallTables(activity: BottomHostActivity) {
            database = initializeDB(activity)
            database!!.getDao().deleteCategorys()
            database!!.getDao().deleteitems()
            database!!.getDao().deleteCartItems()
            database!!.getDao().deleteMulticartItems()
        }


        /* suspend fun getallCustomers(context: Context): List<CustomersItem>? {
             database = initializeDB(context)
             allcustomersFromDb = database!!.getDao().getallCustomers()
             return allcustomersFromDb
         }*/

        fun getallItemsdetail(bottomHostActivity: BottomHostActivity): List<ItemTable>? {
            database = initializeDB(bottomHostActivity)
            itemslistFromDb = database!!.getDao().getallItems()
            return itemslistFromDb
        }
    }


}