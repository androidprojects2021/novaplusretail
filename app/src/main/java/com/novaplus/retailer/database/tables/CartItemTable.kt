package com.novaplus.retailer.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "cart_table")
data class CartItemTable(

    @PrimaryKey(autoGenerate = true)
    var cart_id: Int? = null,

    @ColumnInfo(name = "id")
    var item_id: Int? = null,

    @ColumnInfo(name = "name")
    var item_name: String? = null,

    @ColumnInfo(name = "price")
    var price: Double? = null,

    @ColumnInfo(name = "cost")
    var cost: Double? = null,

    @ColumnInfo(name = "tax")
    var tax: Int? = null,

    @ColumnInfo(name = "cartQty")
    var cartQty: Int? = null,

    @ColumnInfo(name = "discount")
    var discount: Int? = 0,

    @ColumnInfo(name = "customer_id")
    var customer_id: Int? = null,

    @ColumnInfo(name = "customer_name")
    var customer_name: String? = null,

    @ColumnInfo(name = "qty_available")
    var qty_available: Int? = null,

    @ColumnInfo(name = "total")
    var total: Double? = null,


)


