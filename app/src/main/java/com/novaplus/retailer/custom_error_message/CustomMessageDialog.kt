package com.novaplus.plus.common

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.novaplus.retailer.R
import kotlinx.android.synthetic.main.error_dialog_layout.view.*

object CustomMessageDialog {

    @SuppressLint("StaticFieldLeak")
    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog

    @SuppressLint("StaticFieldLeak")
    fun printMessages(message: String, context: Context){
        try {
            builder = AlertDialog.Builder( context )
            customLayout = LayoutInflater.from( context ).inflate(R.layout.error_dialog_layout,null)
            builder.setView(customLayout)
            dialog = builder.create()
            dialog.window?.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)

//            customLayout.text_dialog.text = message.toString()
//
//            customLayout.btn_ok.setOnClickListener { dialog.dismiss() }
            dialog.show()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}