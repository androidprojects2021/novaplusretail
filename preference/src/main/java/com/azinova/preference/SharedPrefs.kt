package com.azinova.preference

import android.content.Context
import android.content.SharedPreferences
import com.azinova.model.cart.CartCommon

import com.google.gson.Gson

object SharedPrefs {


    private lateinit var sharedPreferences: SharedPreferences

    private const val PREF_NAME = "prefName"
    private const val USER_NAME = "username"
    private const val NAME = "name"
    private const val USER_TOKEN = "usertoken"
    private const val LATITUDE = "latitude"
    private const val LONGITUDE = "longitude"
    private const val LOGGEDIN = "login"
    private const val FCM_TOCKEN = "fcmTocken"
    private const val USER_ID = "userid"
    private const val CART_DETAILS = "cartDetails"
    private const val CARTCOUNT = "cart_count"

    fun clearSession(context: Context) {
        val editor =
            context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }

    fun getInstance(context: Context) {
        sharedPreferences =
            context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    var cart_details: CartCommon?
        get() = Gson().fromJson(
            sharedPreferences.getString(CART_DETAILS, ""),
            CartCommon::class.java
        )
        set(id) = sharedPreferences.edit().putString(CART_DETAILS, Gson().toJson(id)).apply()

    var user_name: String?
        get() = sharedPreferences.getString(USER_NAME, "")
        set(id) = sharedPreferences.edit().putString(USER_NAME, id).apply()

    var _name: String?
        get() = sharedPreferences.getString(NAME, "")
        set(id) = sharedPreferences.edit().putString(NAME, id).apply()


    var user_token: String?
        get() = sharedPreferences.getString(USER_TOKEN, "")
        set(value) = sharedPreferences.edit().putString(USER_TOKEN, value).apply()


    var fcm_token: String?
        get() = sharedPreferences.getString(FCM_TOCKEN, "")
        set(value) = sharedPreferences.edit().putString(FCM_TOCKEN, value).apply()


    var login: Boolean
        get() = sharedPreferences.getBoolean(LOGGEDIN, false)
        set(value) = sharedPreferences.edit().putBoolean(LOGGEDIN, value).apply()

    var user_id: String?
        get() = sharedPreferences.getString(USER_ID, "")
        set(value) = sharedPreferences.edit().putString(USER_ID, value).apply()

    var cartcount:Int
        get() = sharedPreferences.getInt(CARTCOUNT,0)
        set(value) = sharedPreferences.edit().putInt(CARTCOUNT,value).apply()


}