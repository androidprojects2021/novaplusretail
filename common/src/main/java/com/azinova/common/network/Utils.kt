package com.azinova.common.network

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*



object Utils {

    @Suppress("SimpleDateFormat")
    fun timeConvert(start: String?, stop: String?):String?{
        val format24 = SimpleDateFormat("HH:mm:ss")
        val format12 = SimpleDateFormat("hh:mm aa")
        var d1: Date? =null
        var d2: Date? =null
        var start12:String?=null
        var stop12:String?=null
        try {

            //Time Taken 24Hrs
            d1= format24.parse(start)
            d2=format24.parse(stop)

            //Time Convert To 12Hrs
            start12 = format12.format(d1)
            stop12 = format12.format(d2)

        }catch (e: Exception){
        }
        return "$start12  To  $stop12"
    }


    @Suppress("SimpleDateFormat")
    fun getDateFormatFromDate(dateandtime: String?): String? {
        var localdate = ""
        val format = SimpleDateFormat("yyyy-MM-dd")
        format.timeZone = TimeZone.getTimeZone("UTC")
        val format2 = SimpleDateFormat("EEEE MMM dd, yyyy")
        format2.timeZone = TimeZone.getTimeZone("GMT+4")
        var date: Date? = null
        try {
//            date = format.parse("2020-09-07 13:05:51");
            date = format.parse(dateandtime)
            localdate = format2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            return localdate
        }
        return localdate
    }

    @Suppress("SimpleDateFormat")
    fun getTimeInFormatFromDateAndTime(dateandtime: String?): String? {
        var localdate = ""
        val format = SimpleDateFormat("MMM dd,yyyy  hh:mm:ss a")
        format.timeZone = TimeZone.getTimeZone("UTC")
        val format2 = SimpleDateFormat("MMM dd,yyyy  hh:mm a")
        format2.timeZone = TimeZone.getTimeZone("GMT+4")
        var date: Date? = null
        try {
            date = format.parse(dateandtime)
            localdate = format2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            return localdate
        }
        return localdate
    }


    fun getDateandTime(unformated_date: String?): String? {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val calendar = Calendar.getInstance()
        var returnDate: String
        val monthNames = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec")
        try {
            val date = simpleDateFormat.parse(unformated_date)
            calendar.time = date
            val AM_PM = calendar[Calendar.AM_PM]
            val am: String
            am = if (AM_PM == 0) {
                "AM"
            } else {
                "PM"
            }
            calendar.timeZone = TimeZone.getDefault()
            returnDate = monthNames[calendar[Calendar.MONTH]] + " " + calendar[Calendar.DAY_OF_MONTH] + ", " + calendar[Calendar.YEAR]
        } catch (e: ParseException) {
            e.printStackTrace()
            returnDate = ""
        }
        return returnDate
    }



}